import 'package:flutter/material.dart';
import '../models/AllUsersData.dart';

class ShowUsers with ChangeNotifier {
  List usersLikeByMe = [];

  List superLikeByMe = [];

  List usersLikeMe = [];

  List usersMatchWithMe = [];

  List removeThisUser = [];

  List<UsersData> allUsersData = [
    UsersData(
      name: 'Anushka Sharma ',
      age: 20,
      imageUrl: [
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRGFFho_w-5ySHGXUpdGjK4u6if9j6CcJAzxg&usqp=CAU",
        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSzLbEevZEuDx1IU9ZQgwHdzf6sZseMyBGt_A&usqp=CAU',
        'https://images.unsplash.com/photo-1518550687729-819219298d98?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80',
        'https://images.unsplash.com/photo-1531891570158-e71b35a485bc?ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=80',
      ],
      email: 'AnushkaSharma@gmail.com',
      isUserActive: true,
      bio: 'Shopping in my heart',
      passion: 'Fashion',
      distance: 21.90,
      city: 'Karnal',
      instagramUserId: 'AnushkaSharma',
    ),
    UsersData(
      name: 'Deepak Mehta ',
      age: 23,
      imageUrl: [
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQLJeIhmkpscyxMLdzLG6jUnstncgD-BQdkvQ&usqp=CAU",
        'https://images.unsplash.com/photo-1529903056346-94341d9428e3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=701&q=80',
      ],
      email: 'DeepakMehta@gmail.com',
      isUserActive: true,
      bio: 'Swag Se Swagat',
      passion: 'Blogger',
      distance: 111.90,
      city: 'Ambala',
      instagramUserId: 'itsDeepakMehta',
    ),
    UsersData(
      name: 'Divansh Sharma ',
      age: 29,
      imageUrl: [
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRNkMijMQuKTW4VorKp8BXxWXGnDvs3OCPghg&usqp=CAU",
        'https://images.unsplash.com/photo-1563765538654-41d961f7db1c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1834&q=80',
      ],
      email: 'DivanshSharma@gmail.com',
      isUserActive: true,
      bio: 'Dream to be a doctor',
      passion: 'Doctor',
      distance: 211.90,
      city: 'Patiala',
      instagramUserId: 'DivanshSharma',
    ),
    UsersData(
      name: 'Ajay Sharma ',
      age: 40,
      imageUrl: [
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSSrEVaHB0QRM3G0lMmGZzCTSaHELiChoLP9w&usqp=CAU",
        'https://images.unsplash.com/photo-1572348805897-a0ca00b77e5b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80',
        'https://images.unsplash.com/photo-1536924430914-91f9e2041b83?ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80',
        'https://images.unsplash.com/photo-1563765538654-41d961f7db1c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80',
      ],
      email: 'AjaySharma@gmail.com',
      isUserActive: false,
      bio: 'Shopping in my heart',
      passion: 'Fashion',
      distance: 21.90,
      city: 'Karnal',
      instagramUserId: 'ThisIsAjaySharma',
    ),
    UsersData(
      name: 'Ankit Mehta ',
      age: 30,
      imageUrl: [
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQBcjOeLVaef71MiQ-sJeVnRvT70H1Dmvw-IQ&usqp=CAU"
      ],
      email: 'AnkitMehta@gmail.com',
      isUserActive: true,
      bio: 'Shopping in my heart',
      passion: 'Fashion',
      distance: 190.0,
      city: 'Karnal',
      instagramUserId: 'AnkitMehta',
    ),
    UsersData(
      name: 'Dev Angad ',
      age: 35,
      imageUrl: [
        "https://i.pinimg.com/originals/e5/6b/79/e56b799b365e63c41041feb38fb7e965.jpg"
      ],
      email: 'DevAngad@gmail.com',
      isUserActive: false,
      bio: 'Shopping in my heart',
      passion: 'Fashion',
      distance: 16.80,
      city: 'Karnal',
      instagramUserId: 'DevAngad',
    ),
    UsersData(
      name: 'Suraj Kamra ',
      age: 32,
      imageUrl: [
        "https://static.toiimg.com/thumb/msid-75380539,width-900,height-1200,resizemode-6.cms"
      ],
      email: 'SurajKamra@gmail.com',
      isUserActive: true,
      bio: 'Shopping in my heart',
      passion: 'Fashion',
      distance: 121.90,
      city: 'Panipat',
      instagramUserId: 'SurajKamra',
    ),
    UsersData(
      name: 'Angad Mishra ',
      age: 22,
      imageUrl: [
        "https://static01.nyt.com/images/2018/02/08/t-magazine/08tmag-models-slide-9EPC/08tmag-models-slide-9EPC-superJumbo.jpg"
      ],
      email: 'AngadMishra@gmail.com',
      isUserActive: true,
      bio: 'Shopping in my heart',
      passion: 'Fashion',
      distance: 116.90,
      city: 'Chandigarh',
      instagramUserId: 'AngadMishra',
    ),
    UsersData(
      name: 'Sanjay Ahuja ',
      age: 26,
      imageUrl: [
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRGFFho_w-5ySHGXUpdGjK4u6if9j6CcJAzxg&usqp=CAU"
      ],
      email: 'SanjayAhuja@gmail.com',
      isUserActive: false,
      bio: 'Shopping in my heart',
      passion: 'Fashion',
      distance: 21.90,
      city: 'Karnal',
      instagramUserId: 'SanjayAhuja',
    ),
    UsersData(
      name: 'Vijay Kathpal ',
      age: 28,
      imageUrl: [
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRNkMijMQuKTW4VorKp8BXxWXGnDvs3OCPghg&usqp=CAU"
      ],
      email: 'VijayKathpal@gmail.com',
      isUserActive: true,
      bio: 'Shopping in my heart',
      passion: 'Fashion',
      distance: 21.90,
      city: 'Karnal',
      instagramUserId: 'VijayKathpal',
    ),
    UsersData(
      name: 'Nisha Mittal ',
      age: 29,
      imageUrl: [
        "https://i.pinimg.com/originals/e5/6b/79/e56b799b365e63c41041feb38fb7e965.jpg"
      ],
      email: 'NishaMittal@gmail.com',
      isUserActive: true,
      bio: 'Shopping in my heart',
      passion: 'Fashion',
      distance: 21.90,
      city: 'Mumbai',
      instagramUserId: 'NishaMittal',
    ),
    UsersData(
      name: 'Sarika Chauhan ',
      age: 30,
      imageUrl: [
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQLJeIhmkpscyxMLdzLG6jUnstncgD-BQdkvQ&usqp=CAU"
      ],
      email: 'SarikaChauhan@gmail.com',
      isUserActive: true,
      bio: 'Shopping in my heart',
      passion: 'Fashion',
      distance: 90.60,
      city: 'Chandigarh',
      instagramUserId: 'SarikaChauhan',
    ),
  ];

  List<UsersData> get allUsers {
    return allUsersData
        .where(
          (user) =>
              !removeThisUser.contains(user.email) &&
              !usersLikeByMe.contains(user.email) &&
              !superLikeByMe.contains(user.email),
        )
        .toList();
  }

  List get showAllUserOnHome {
    return allUsers.toList();
  }

  List get showRandomActiveUser {
    return allUsers
        .where((user) =>
            user.isUserActive == true && !allUsers.contains(user.email))
        .toList();
  }

  void doNotShowThisUser(String userEmailId) {
    removeThisUser.insert(0, userEmailId);
    notifyListeners();
    print('Removed User Successfully');
  }

  void likeThisUser(String userEmail) {
    usersLikeByMe.insert(0, userEmail);
    notifyListeners();
    print('Liked User Successfully');
  }

  void superLikeThisUser(String userId) {
    superLikeByMe.insert(0, userId);
    notifyListeners();
    print('SuperLiked User Successfully');
  }
}
