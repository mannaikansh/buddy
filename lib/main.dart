import 'dart:convert';
import 'Providers/ProfileProvider.dart';

import './screens/all_screen.dart';
import 'Data/ShowData.dart';
import 'helpers/http/httpRequest.dart';
import 'package:provider/provider.dart';

import 'Providers/HomeProvider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => ShowUsers(),
        ),
        ChangeNotifierProvider(
          create: (_) => HomeProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => ProfileProvider(),
        )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Buddy',
        theme: ThemeData(
          primarySwatch: Colors.orange,
          primaryColor: Colors.black,
          accentColor: Colors.white,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          canvasColor: Colors.white,
        ),
        initialRoute: LoginScreen.routeName,
        routes: {
          LoginScreen.routeName: (ctx) => LoginScreen(),
          //'interactionScreen'
          Interaction.routeName: (ctx) => Interaction(),
          //'homeScreen'
          Bottom.routeName: (ctx) => Bottom(),
          //'adsScreen'
          Advertise.routeName: (ctx) => Advertise(),
          // 'messageScreen'
          MessagesScreen.routeName: (ctx) => MessagesScreen(),
          //'chatScreen'
          ChatScreen.routeName: (ctx) => ChatScreen(),
          //'profileScreen'
          ProfilePage.routeName: (ctx) => ProfilePage(),
          //'editProfileScreen'
          EditProfile.routeName: (ctx) => EditProfile(),
          //'filtersScreen'
          FiltersScreen.routeName: (ctx) => FiltersScreen(),
          // 'privacyPolicyScreen'
          PrivacyPolicyScreen.routeName: (ctx) => PrivacyPolicyScreen(),
          // 'aboutUsScreen'
          AboutUsScreen.routeName: (ctx) => AboutUsScreen(),
          // 'helpScreen'
          HelpScreen.routeName: (ctx) => HelpScreen(),
          // 'faqScreen'
          FAQScreen.routeName: (ctx) => FAQScreen(),
          // 'supportScreen'
          SupportScreen.routeName: (ctx) => SupportScreen(),
          // 'Homepage'
          MyHomePage.routeName: (ctx) => MyHomePage(),
          // 'SettingsScreen'
          SettingsScreen.routeName: (ctx) => SettingsScreen(),
        },
      ),
    );
  }
}

class Bottom extends StatefulWidget {
  static const routeName = "/home-screen";
  @override
  _BottomState createState() => _BottomState();
}

class _BottomState extends State<Bottom> {
  int _currentindex = 0;
  bool _isInit = true;
  void onTapped(int index) {
    setState(() {
      _currentindex = index;
    });
  }

  void _checkApi() async {
    try {
      // SharedPreferences prefs = await SharedPreferences.getInstance();
      // String emp = prefs.getString("usern");

      final url = "http://suburl.in/api/match";
      final response =
          await HttpRequestProcess().requestProcess(context, url, "get");
      response
          .transform(utf8.decoder)
          .transform(json.decoder)
          .listen((contents) async {
        print('$contents : contents');
        if (response.statusCode == 200) {
          Map userMap = contents;
          print(userMap);
        } else {
          print(response.reasonPhrase);
        }
      });
    } catch (e) {
      // print('error caught: $e');
      print(e);
    }
  }

  Widget bottomBar() {
    return BottomNavigationBar(
      elevation: 10,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.explore),
          // ignore: deprecated_member_use
          title: Text('Home'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.favorite),
          // ignore: deprecated_member_use
          title: Text('Interaction'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.dashboard),
          // ignore: deprecated_member_use
          title: Text('Ads'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person_outline),
          // ignore: deprecated_member_use
          title: Text('Profile'),
        ),
      ],
      currentIndex: _currentindex,
      selectedItemColor: Theme.of(context).primaryColor,
      unselectedItemColor: Colors.grey,
      iconSize: 35,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      onTap: onTapped,
      backgroundColor: Colors.white,
      type: BottomNavigationBarType.fixed,
    );
  }

  List pages = [
    MyHomePage(),
    Interaction(),
    Advertise(),
    SettingsScreen(),
  ];

  @override
  void initState() {
    // Future.delayed(Duration.zero).then((_) async {
    //   await ;
    // });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: bottomBar(),
      body: pages[_currentindex],
    );
  }
}
