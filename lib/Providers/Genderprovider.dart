import 'dart:convert';

import 'package:flutter/material.dart';

import '../models/GenderModel.dart';
import '../helpers/constants.dart';
import '../helpers/http/httpRequest.dart';

class GenderData with ChangeNotifier {
  GenderModel _gdm;

  GenderModel get getGenders {
    return _gdm;
  }

  Future<void> allGenders(context) async {
    GenderModel gdm;
    try {
      var url = "$baseUrl/home";
      final response =
          await HttpRequestProcess().requestProcess(context, url, "post");
      response.transform(utf8.decoder).transform(json.decoder).listen(
          (contents) async {
            if (response.statusCode == 200) {
              Map userMap = contents;
              print('Gender : $userMap');
              if (userMap["code"] == 1) {
                gdm = GenderModel.fromJson(userMap);
              } else {
                gdm = GenderModel(
                  code: 0,
                  message: "No Genders Available.",
                  genderList: null,
                );
              }
            } else {
              gdm = GenderModel(
                code: 0,
                message: response.reasonPhrase,
                genderList: null,
              );
            }
          },
          onDone: () => _gdm = gdm,
          onError: (err) {
            _gdm = GenderModel(
              code: 0,
              message: err.toString(),
              genderList: null,
            );
          },
          cancelOnError: true);
    } catch (e) {
      _gdm = GenderModel(
        code: 0,
        message: e.toString(),
        genderList: null,
      );
    }
    notifyListeners();
  }
}
