import 'dart:convert';

import '../screens/all_screen.dart';
import '../models/ContactListModel.dart';
import '../helpers/constants.dart';
import '../helpers/http/httpRequest.dart';

class ContactProvider with ChangeNotifier {
  ContactModel _cm;

  ContactModel get getContacts => _cm;

  Future<void> getContactsDetail(context, userId) async {
    var url = "$baseUrl/random";
    var params = {"id": userId};
    try {
      final response = await HttpRequestProcess()
          .requestProcessWithParams(context, url, "post", params);
      response.transform(utf8.decoder).transform(json.decoder).listen(
            (contents) {
              if (response.statusCode == 200) {
                Map userMap = contents;
                print('userMap : $userMap');
                if (userMap["code"] == 1) {
                  _cm = ContactModel.fromJson(userMap);
                } else {
                  _cm = ContactModel(
                    code: 0,
                    message: "No Data Found.",
                    contact: null,
                  );
                }
              } else {
                _cm = ContactModel(
                  code: 0,
                  message: response.reasonPhrase,
                  contact: null,
                );
              }
            },
            onDone: () => _cm.contact.addAll(_cm.contact),
            onError: (err) {
              _cm = ContactModel(
                code: 0,
                message: err.toString(),
                contact: null,
              );
            },
            cancelOnError: true,
          );
    } catch (error) {
      print('Error : $error');
      _cm = ContactModel(
        code: 0,
        message: error,
        contact: null,
      );
    }
    notifyListeners();
  }
}
