import 'dart:convert';

import 'package:flutter/material.dart';
import '../models/HomeModel.dart';
import '../helpers/constants.dart';
import '../helpers/http/httpRequest.dart';

class HomeProvider with ChangeNotifier {
  HomeModel _users;

  HomeModel get getUsers => _users;

  Future<void> allUsers(context) async {
    
    var url = "$baseUrl/home";
    try {
      final response =
          await HttpRequestProcess().requestProcess(context, url, "get");
      response.transform(utf8.decoder).transform(json.decoder).listen(
            (contents) {
              if (response.statusCode == 200) {
                Map userMap = contents;
                if (userMap["code"] == 1) {
                  _users = HomeModel.fromJson(userMap);
                } else {
                  _users = HomeModel(
                    code: 0,
                    message: "No Data Found.",
                    homeUserList: null,
                  );
                }
              } else {
                _users = HomeModel(
                  code: 0,
                  message: response.reasonPhrase,
                  homeUserList: null,
                );
              }
            },
            onDone: () => _users = _users,
            onError: (err) {
              _users = HomeModel(
                code: 0,
                message: err.toString(),
                homeUserList: null,
              );
            },
            cancelOnError: true,
          );
    } catch (error) {
      print('Error : $error');
      _users = HomeModel(
        code: 0,
        message: error,
        homeUserList: null,
      );
    }
    notifyListeners();
  }
}
