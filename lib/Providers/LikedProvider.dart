import 'dart:convert';

import '../screens/all_screen.dart';
import '../models/LikedModel.dart';
import '../helpers/constants.dart';
import '../helpers/http/httpRequest.dart';

class LikedProvider with ChangeNotifier {
  LikedModel _lm;

  LikedModel get getLikedUsers => _lm;

  Future<void> getLikedDetail(context, userId) async {
    var url = "$baseUrl/random";
    var params = {"id": userId};
    try {
      final response = await HttpRequestProcess()
          .requestProcessWithParams(context, url, "post", params);
      response.transform(utf8.decoder).transform(json.decoder).listen(
            (contents) {
              if (response.statusCode == 200) {
                Map userMap = contents;
                print('userMap : $userMap');
                if (userMap["code"] == 1) {
                  _lm = LikedModel.fromJson(userMap);
                } else {
                  _lm = LikedModel(
                    code: 0,
                    message: "No Data Found.",
                    liked: null,
                  );
                }
              } else {
                _lm = LikedModel(
                  code: 0,
                  message: response.reasonPhrase,
                  liked: null,
                );
              }
            },
            onDone: () => _lm.liked.addAll(_lm.liked),
            onError: (err) {
              _lm = LikedModel(
                code: 0,
                message: err.toString(),
                liked: null,
              );
            },
            cancelOnError: true,
          );
    } catch (error) {
      print('Error : $error');
      _lm = LikedModel(
        code: 0,
        message: error,
        liked: null,
      );
    }
    notifyListeners();
  }
}
