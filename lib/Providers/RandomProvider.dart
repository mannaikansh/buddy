import 'dart:convert';

import '../screens/all_screen.dart';
import '../models/RandomUserModel.dart';
import '../helpers/constants.dart';
import '../helpers/http/httpRequest.dart';

class RandomProvider with ChangeNotifier {
  RandomUserModel _rm;

  RandomUserModel get getRandomUsers => _rm;

  Future<void> getRandomUserDetail(context, userId) async {
    var url = "$baseUrl/random";
    var params = {"id": userId};
    try {
      final response = await HttpRequestProcess()
          .requestProcessWithParams(context, url, "post", params);
      response.transform(utf8.decoder).transform(json.decoder).listen(
            (contents) {
              if (response.statusCode == 200) {
                Map userMap = contents;
                print('userMap : $userMap');
                if (userMap["code"] == 1) {
                  _rm = RandomUserModel.fromJson(userMap);
                } else {
                  _rm = RandomUserModel(
                    code: 0,
                    message: "No Data Found.",
                    randomUser: null,
                  );
                }
              } else {
                _rm = RandomUserModel(
                  code: 0,
                  message: response.reasonPhrase,
                  randomUser: null,
                );
              }
            },
            onDone: () => _rm.randomUser.addAll(_rm.randomUser),
            onError: (err) {
              _rm = RandomUserModel(
                code: 0,
                message: err.toString(),
                randomUser: null,
              );
            },
            cancelOnError: true,
          );
    } catch (error) {
      print('Error : $error');
      _rm = RandomUserModel(
        code: 0,
        message: error,
        randomUser: null,
      );
    }
    notifyListeners();
  }
}
