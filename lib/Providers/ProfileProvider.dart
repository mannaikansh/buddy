import 'dart:convert';

import '../screens/all_screen.dart';
import '../models/ProfileModel.dart';
import '../helpers/constants.dart';
import '../helpers/http/httpRequest.dart';

class ProfileProvider with ChangeNotifier {
  ProfileModel _pm;

  ProfileModel get getProfile => _pm;

  Future<void> getProfileDetail(context, userId) async {
    var url = "$baseUrl/user"; //5f79815bd80f9b26c84864e9
    print('Id :  $userId');
    var params = {"id": userId.toString()};
    try {
      final response = await HttpRequestProcess()
          .requestProcessWithParams(context, url, "post", params);
      response.transform(utf8.decoder).transform(json.decoder).listen(
            (contents) {
              if (response.statusCode == 200) {
                Map userMap = contents;
                print('userMap : $userMap');
                if (userMap["code"] == 1) {
                  _pm = ProfileModel.fromJson(userMap);
                } else {
                  _pm = ProfileModel(
                    code: 0,
                    message: "No Profile Found.",
                    profile: null,
                  );
                }
              } else {
                _pm = ProfileModel(
                  code: 0,
                  message: response.reasonPhrase,
                  profile: null,
                );
              }
            },
            onDone: () => _pm = _pm,
            onError: (err) {
              _pm = ProfileModel(
                code: 0,
                message: err.toString(),
                profile: null,
              );
            },
            cancelOnError: true,
          );
    } catch (error) {
      print('Error : $error');
      _pm = ProfileModel(
        code: 0,
        message: error,
        profile: null,
      );
    }
    notifyListeners();
  }
}
