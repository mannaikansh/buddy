import 'package:flutter/material.dart';
import '../helpers/constants.dart' as constant;

class LoadingModal extends StatefulWidget {
  @override
  _LoadingModalState createState() => _LoadingModalState();
}

class _LoadingModalState extends State<LoadingModal> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SizedBox(
            height: 40.0,
            width: 40.0,
            child: CircularProgressIndicator(
                backgroundColor: Colors.black,
                valueColor:
                    AlwaysStoppedAnimation<Color>(constant.spinnerColor),
                strokeWidth: 5.0),
          )
        ],
      ),
    );
  }
}
