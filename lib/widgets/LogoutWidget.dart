import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

// import '../LoginScreen.dart';
// import '../PinActivity.dart';

class LogoutItem extends StatelessWidget {
  setPre() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('pinstate', "notsaved");
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.maxFinite,
      padding: EdgeInsets.fromLTRB(25.0, 25.0, 25.0, 25.0),
      //padding: EdgeInsets.only(top: 25.0),
      decoration: BoxDecoration(
        color: Color(0xff85C1E9),
      ),

      // padding: EdgeInsets.all(25.0),
      child: Center(
          child: Card(
        color: Colors.white,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Flexible(
              flex: 2,
              child: Text(
                "Are You Sure Want To Logout?",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
            ),
            Flexible(
                flex: 3,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child: MaterialButton(
                        color: Colors.blue,
                        textColor: Colors.white,
                        child: Text("Yes"),
                        onPressed: () {
                          setPre();
                          // prefs.setString('pinto',pin);
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(builder: (context) {
                              return;
                            } // LoginScreen(),
                                ),
                          );
                        },
                      ),
                    )
                  ],
                )),
            Expanded(
                flex: 3,
                child: MaterialButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  child: Text("Cancel"),
                  onPressed: () {
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) {
                      return;
                    })); // PinActivity()));
                  },
                ))
          ],
        ),
      )),
    );
  }
}
