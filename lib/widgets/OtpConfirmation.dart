import 'package:flutter/material.dart';

class OtpConfirmation extends StatefulWidget {
  final Function function;

  OtpConfirmation({this.function});
  @override
  _OtpConfirmationState createState() => _OtpConfirmationState();
}

class _OtpConfirmationState extends State<OtpConfirmation> {
  final otpValue = new TextEditingController();
  bool _isError = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Container(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        // height: MediaQuery.of(context).size.height * .2,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              color: Theme.of(context).accentColor,
              width: double.infinity,
              padding: const EdgeInsets.all(15),
              child: Text(
                "OTP CONFIRMATION",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    // color: Colors.white,
                    fontSize: 16),
              ),
            ),
            TextField(
              controller: otpValue,
              decoration: InputDecoration(
                  counterText: '', hintText: "Please Enter Your 6 digit OTP."),
              obscureText: true,
              textAlign: TextAlign.center,
              maxLength: 6,
              cursorColor: Theme.of(context).primaryColor,
              keyboardType: TextInputType.number,
            ),
            _isError
                ? Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Please Enter a valid Otp Value.",
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ))
                : SizedBox(
                    height: 10,
                  ),
            RaisedButton(
              color: Theme.of(context).primaryColor,
              child: Text(
                "Confirm OTP",
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              onPressed: () {
                if (otpValue.text.isEmpty || otpValue.text.length < 6) {
                  setState(() {
                    _isError = true;
                  });
                } else {
                  setState(() {
                    _isError = false;
                  });
                  widget.function(otpValue.text);
                }
              },
            )
          ],
        ),
      ),
    );
  }
}
