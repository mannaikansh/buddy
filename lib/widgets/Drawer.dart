import '../screens/all_screen.dart';

class DrawerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
            child: ListTile(
              leading: Container(
                  decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      shape: BoxShape.circle),
                  child: IconButton(
                    icon: Icon(Icons.person),
                    onPressed: () {
                      Navigator.of(context).pushNamed(EditProfile.routeName);
                    },
                    color: Colors.white,
                  )),
              title: Text(
                'Munish Mehta',
                style: TextStyle(fontSize: 17),
              ),
              trailing: IconButton(
                  icon: Icon(Icons.mode_edit),
                  onPressed: () {
                    Navigator.of(context).pushNamed(EditProfile.routeName);
                  }),
            ),
          ),
          Container(
            decoration: BoxDecoration(color: Theme.of(context).primaryColor),
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'UPGRADE NOW',
                  style: TextStyle(
                      color: Theme.of(context).canvasColor,
                      fontSize: 25,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  'Chat, Send Messages and connect instant',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                  ),
                ),
              ],
            ),
          ),
          ListTile(
            leading: ImageIcon(
              AssetImage('assets/images/chat.png'),
              size: 25,
              color: Theme.of(context).primaryColor,
            ),
            title: Text('Messages'),
            onTap: () {
              Navigator.of(context).pushNamed(MessagesScreen.routeName);
            },
          ),
          ListTile(
            leading: Icon(
              Icons.verified_user,
              size: 25,
              color: Theme.of(context).primaryColor,
            ),
            title: Text('Privacy Policy'),
            onTap: () {
              Navigator.of(context).pushNamed(PrivacyPolicyScreen.routeName);
            },
          ),
          ListTile(
            leading: Icon(
              Icons.info,
              color: Theme.of(context).primaryColor,
              size: 25,
            ),
            title: Text('About Us'),
            onTap: () {
              Navigator.of(context).pushNamed(AboutUsScreen.routeName);
            },
          ),
          ListTile(
            leading: Icon(
              Icons.live_help,
              color: Theme.of(context).primaryColor,
              size: 25,
            ),
            title: Text('Help'),
            onTap: () {
              Navigator.of(context).pushNamed(HelpScreen.routeName);
            },
          ),
          ListTile(
            leading: Icon(
              Icons.contacts,
              color: Theme.of(context).primaryColor,
              size: 25,
            ),
            title: Text('Support'),
            onTap: () {
              Navigator.of(context).pushNamed(SupportScreen.routeName);
            },
          ),
          ListTile(
            leading: Icon(
              Icons.format_quote,
              color: Theme.of(context).primaryColor,
              size: 25,
            ),
            title: Text('FAQ'),
            onTap: () {
              Navigator.of(context).pushNamed(FAQScreen.routeName);
            },
          ),
        ],
      ),
    );
  }
}
