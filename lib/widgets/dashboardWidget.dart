// import 'package:flutter/material.dart';
// import '../models/categoryModel.dart';
// import '../screens/categoryMeals.dart';

// class DashboardWidget extends StatelessWidget {
//   final CategoryModel category;

//   CategoryItem(this.category);

//   @override
//   Widget build(BuildContext context) {
//     return InkWell(
//       onTap: () {
//         print(category.id);
//         Navigator.of(context).pushReplacement(
//           MaterialPageRoute(
//             builder: (_) {
//               return CategoryMealScreen();
//             },
//           ),
//         );
//       },
//       splashColor: Theme.of(context).primaryColor,
//       borderRadius: BorderRadius.circular(15),
//       child: Container(
//         padding: EdgeInsets.all(15),
//         decoration: BoxDecoration(
//           gradient: LinearGradient(colors: [
//             category.categoryColor.withOpacity(0.4),
//             category.categoryColor,
//           ], begin: Alignment.topLeft, end: Alignment.bottomRight),
//           borderRadius: BorderRadius.circular(15),
//         ),
//         child: Center(
//           child: Text(
//             category.categoryTitle,
//             style: Theme.of(context).textTheme.headline5,
//           ),
//         ),
//       ),
//     );
//   }
// }
