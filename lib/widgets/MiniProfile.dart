import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../helpers/common.dart';

class MiniProfile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Common().launchURL(
          "https://www.amazon.in/ref=as_li_ss_tl?ie=UTF8&linkCode=ll2&tag=shopy20-21&linkId=d82141ee80281f15d2c824e1d13e0354&language=en_IN"),
      child: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ),
        child: Center(
          child: FaIcon(
            FontAwesomeIcons.userAlt,
            color: Theme.of(context).accentColor,
          ),
        ),
      ),
    );
  }
}
