import 'package:flutter/material.dart';

class UsersData with ChangeNotifier {
  final String name;
  final int age;
  final List<String> imageUrl;
  final String email;
  final String bio;
  final String passion;
  final double distance;
  final String city;
  final String instagramUserId;
  final bool isUserActive;

  UsersData({
    @required this.name,
    @required this.age,
    @required this.imageUrl,
    @required this.email,
    @required this.bio,
    @required this.passion,
    @required this.distance,
    @required this.city,
    @required this.instagramUserId,
    @required this.isUserActive,
  });
}
