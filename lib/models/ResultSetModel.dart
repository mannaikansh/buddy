class ResultSet {
  int code;
  String message;

  ResultSet({this.code, this.message});
  factory ResultSet.fromJson(Map<String, dynamic> json) {
    return ResultSet(
      code: json["code"],
      message: json["message"],
    );
  }
}
