import 'ResultSetModel.dart';

class GenderModel extends ResultSet {
  int code;
  String message;
  List<GenderDetailModel> genderList;

  GenderModel({this.code, this.message, this.genderList});

  factory GenderModel.fromJson(Map<String, dynamic> json) {
    final code = json["code"],
        message = json["message"],
        genderList = List.from(json["data"])
            .map((item) => GenderDetailModel.fromJson(item))
            .toList();
    return GenderModel(
      code: code,
      message: message,
      genderList: genderList,
    );
  }
}

class GenderDetailModel {
  String genderAbbrevation;
  String genderName;

  GenderDetailModel.fromJson(Map<String, dynamic> json)
      : genderAbbrevation = json["genderAbbrevation"],
        genderName = json["genderName"];
}
