import 'ResultSetModel.dart';

class RandomUserModel extends ResultSet {
  int code;
  String message;
  List<HomeUserModel> randomUser;

  RandomUserModel({this.code, this.message, this.randomUser});

  factory RandomUserModel.fromJson(Map<String, dynamic> json) {
    print('json : $json');
    final code = json["code"],
        message = json["message"],
      randomUser = List.from(json["data"])
            .map((item) => HomeUserModel.fromJson(item))
            .toList();
    return RandomUserModel(
      code: code,
      message: message,
      randomUser: randomUser,
    );
  }
}

class HomeUserModel {
  String userName;
  int userAge;
  String userAbout;
  List<UserImages> images;
  HomeUserModel.fromJson(Map<String, dynamic> json)
      : userName = json["name"],
        userAge = json["age"],
        userAbout = json["about"],
        images = List.from(json["passions"])
            .map((item) => UserImages.fromJson(item))
            .toList();
}

class UserImages {
  String id;
  String url;

  UserImages.fromJson(Map<String, dynamic> json)
      : id = json["_id"],
        url = json["url"];
}
