import 'dart:ffi';

import './ResultSetModel.dart';

class ContactModel extends ResultSet {
  int code;
  String message;
  List<Contact> contact;

  ContactModel({this.code, this.message, this.contact});

  factory ContactModel.fromJson(Map<String, dynamic> json) {
    print('json : $json');
    final code = json["code"],
        message = json["message"],
        contact = List.from(json["data"])
            .map((item) => Contact.fromJson(item))
            .toList();
    return ContactModel(
      code: code,
      message: message,
      contact: contact,
    );
  }
}

class Contact {
  String contactId;
  Uint64 image;
  bool isOnline;
  Contact.fromJson(Map<String, dynamic> json)
      : contactId = json["contactId"],
        image = json["image"],
        isOnline = json["isOnline"];
}
