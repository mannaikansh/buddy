import 'dart:ffi';

import './ResultSetModel.dart';

class LikedModel extends ResultSet {
  int code;
  String message;
  List<Liked> liked;

  LikedModel({this.code, this.message, this.liked});

  factory LikedModel.fromJson(Map<String, dynamic> json) {
    print('json : $json');
    final code = json["code"],
        message = json["message"],
        liked = List.from(json["data"])
            .map((item) => Liked.fromJson(item))
            .toList();
    return LikedModel(
      code: code,
      message: message,
      liked: liked,
    );
  }
}

class Liked {
  String contactId;
  Uint64 image;
  bool isOnline;
  Liked.fromJson(Map<String, dynamic> json)
      : contactId = json["likedId"],
        image = json["image"],
        isOnline = json["isOnline"];
}
