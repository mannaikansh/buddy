import 'ResultSetModel.dart';

class HomeModel extends ResultSet {
  int code;
  String message;
  List<HomeUserModel> homeUserList;

  HomeModel({this.code, this.message, this.homeUserList});

  factory HomeModel.fromJson(Map<String, dynamic> json) {
    print('json : $json');
    final code = json["code"],
        message = json["message"],
        homeUserList = List.from(json["data"])
            .map((item) => HomeUserModel.fromJson(item))
            .toList();
    return HomeModel(
      code: code,
      message: message,
      homeUserList: homeUserList,
    );
  }
}

class HomeUserModel {
  String userName;
  int userAge;
  String userAbout;
  List<UserImages> images;
  HomeUserModel.fromJson(Map<String, dynamic> json)
      : userName = json["name"],
        userAge = json["age"],
        userAbout = json["about"];
        //images = List.from(json["passions"])
          //  .map((item) => UserImages.fromJson(item))
            //.toList();
}

class UserImages {
  String id;
  String url;

  UserImages.fromJson(Map<String, dynamic> json)
      : id = json["_id"],
        url = json["url"];
}
