import 'ResultSetModel.dart';

class ProfileModel extends ResultSet {
  int code;
  String message;
  List<ProfileDataModel> profile;

  ProfileModel({this.code, this.message, this.profile});

  factory ProfileModel.fromJson(Map<String, dynamic> json) {
    final code = json["code"],
        message = json["message"],
        profile = List.from(json["data"])
            .map((item) => ProfileDataModel.fromJson(item))
            .toList();
    return ProfileModel(
      code: code,
      message: message,
      profile: profile,
    );
  }
}

class ProfileDataModel {
  String userName;
  String userImage;
  bool isVerified;
  int userAge;

  ProfileDataModel.fromJson(Map<String, dynamic> json)
      : userName = json["name"],
        userImage = json["userImage"],
        isVerified = json["isVerified"],
        userAge = json["age"];
}
