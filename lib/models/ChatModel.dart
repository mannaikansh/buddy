import 'dart:ffi';

import './ResultSetModel.dart';

class ChatModel extends ResultSet {
  int code;
  String message;
  List<Chatting> chatting;

  ChatModel({this.code, this.message, this.chatting});

  factory ChatModel.fromJson(Map<String, dynamic> json) {
    print('json : $json');
    final code = json["code"],
        message = json["message"],
        chatting = List.from(json["data"])
            .map((item) => Chatting.fromJson(item))
            .toList();
    return ChatModel(
      code: code,
      message: message,
      chatting: chatting,
    );
  }
}
class Chatting {
  String senderId;
  String receiverId;
  String content;
  Uint64 attachment;
  Chatting.fromJson(Map<String, dynamic> json)
      : senderId = json["senderId"],
        receiverId = json["receiverId"],
        content = json["content"],
        attachment = json["attachment"];
}
