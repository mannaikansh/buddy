import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:base16/base16.dart';
import 'package:crypto/crypto.dart';
import 'package:encrypt/encrypt.dart' as en;
import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'ChipperWay.dart';

class EncrptMe {
  get cipher2 => null;

  ChipperWay ch = new ChipperWay();

  Future<String> encrypt(String value) async {
    String chabbi = '8837877275abcdef';
    String vect = 'patial8837877275';

    Future<String> encryptedString =
        ChipperWay.encryptAesCbc128Padding7(value, chabbi, vect);

    return encryptedString;
  }

  void secondMethod(String somestring) {
    final key = en.Key.fromUtf8('8837877275abcdef');
    final iv = en.IV.fromUtf8('patial8837877275');

    final encrypter = en.Encrypter(en.AES(key));

    final encrypted = encrypter.encrypt(somestring, iv: iv);
    // ignore: unused_local_variable
    final decrypted = encrypter.decrypt(encrypted, iv: iv);
  }

  Future<String> thirdMethod(String hh) async {
    final key = en.Key.fromUtf8('8837877275abcdef');
    final iv = en.IV.fromUtf8('patial8837877275');

    print('$key : key, $iv : iv');

    return (en.Encrypter(en.AES(key, mode: en.AESMode.cbc))
        .encrypt(hh, iv: iv)
        .base64);
  }

  String onlydecr(String comingvalue) {
    final key = en.Key.fromUtf8('8837877275abcdef');
    final iv = en.IV.fromUtf8('patial8837877275');
    final encrypter = en.Encrypter(en.AES(key, mode: en.AESMode.cbc));
    final decrypted = encrypter.decrypt64(comingvalue, iv: iv);
    //encrypter.decrypt64(encoded)
    return decrypted.toString();
  }

  // made random genrated token bytes
  Future<String> genratedToken(String token) async {
    // long time= System.currentTimeMillis();
    var ms = (new DateTime.now()).millisecondsSinceEpoch;
    // String timeinsec=String.valueOf(ms);
    List<int> bytes = utf8.encode(ms.toString());
    //print(bytes);
    //var values = List<int>.generate(16, (i) => bytes.(256));
    //var gg= base16encode(Uint8List.fromList(bytes));
    String encoded = base64Encode(bytes); //encode(Uint8List.fromList(values));
//print(encoded.length);
    // String encoded=returnEnoded(ms);
    String reverseEng = swap(encoded.toString().replaceAll(" ", "").trim());

//print(token+"time stmp"+reverseEng);
    String s = returnMixupString(token, reverseEng);
    String h = reverseEng.length.toString();
//print(s);
    return s + " " + h;
  }

  String swap(String item) {
    final Random random = new Random();
    final StringBuffer sb = new StringBuffer(item.length);
    // demo.write(token[y]);
    for (int i = 0; i < item.length; ++i)
      sb.write(item[random.nextInt(item.length)]);
    return sb.toString();
  }

  Future<HttpClient> genrateClient(BuildContext context) async {
    final SecurityContext serverSecurityContext = new SecurityContext();
    //final List<int> certificateChainBytes = (await AssetBundle.load('your asset url')).buffer.asInt8List();
    // context.useCertificateChainBytes(certificateChainBytes);
    // String data =
    //     await DefaultAssetBundle.of(context).loadString('assets/server.crt');
    // //final ByteData crtData = await rootBundle.load('assets/raw/Id.crt');
    // List bytes = utf8.encode(data);
    // serverSecurityContext.setTrustedCertificatesBytes(bytes);
    // serverSecurityContext.useCertificateChainBytes(
    // await new File('assets/raw/Id.crt').readAsBytes());
    var client = HttpClient(context: serverSecurityContext);

    // client.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    return client;
  }

  Future<String> getSalt() async {
    //HexEncoder _hexEncoder;
    var random = Random.secure();

    var values = List<int>.generate(16, (i) => random.nextInt(256));
    //int.toRadixString(16)
    // var bb= hex.encode(values.toString().to);
    //var decoded = base32.decode("JBSWY3DPEHPK3PXP");

    // final Uint8List _objectIDBytes = Uint8List.fromList(const [142, 71, 191, 93, 246, 100, 21, 15, 0, 205, 16, 37]);

    //void main() => print(base16encode(_objectIDBytes));
    //final Uint8List _objectIDBytes = Uint8List.fromList(const(values));
    var decodedHex = base16encode(Uint8List.fromList(values));
    //String ss= base64Url.encode(values);
    return decodedHex.toString().toUpperCase();
  }

  Future<String> get256String(String firsttime) async {
    var bytes = utf8.encode(firsttime);
    // data being hashed
    // Sha256 sha256;
    //final sha256 =   Sha256._();
    //Sha256 newInstance() => Sha256._();
    // Sha256
    var digest = sha256.convert(bytes);
    print('$digest : digest');
    //print("Digest as bytes: ${digest.bytes}");
    //print("Digest as hex string: $digest");
    return getFinalHex(digest.toString().toUpperCase());
  }

  Future<String> getFinalHex(var e) async {
    String sa = await getSalt();
    var bytes = utf8.encode(e + sa);

    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('salt : $sa');
    prefs.setString('salt_value', sa);

    var digest = sha256.convert(bytes);

    return digest.toString().toUpperCase();
  }

  /*swap(String encoded) {
    final Random random=new Random();
    final StringBuffer  sb=new StringBuffer (encoded.length);
    for(int i=0;i<encoded.length;++i)
      sb.write(encoded.)
      sb.append(str.charAt(random.nextInt(str.length())));
    return sb.toString();
  }*/

  /* Uint8List derivedKey = deriveKey(password);
  KeyParameter keyParam = new KeyParameter(derivedKey);
  BlockCipher aes = new AESFastEngine();

  var rnd = FortunaRandom();
  rnd.seed(keyParam);
  Uint8List iv = rnd.nextBytes(aes.blockSize);

  BlockCipher cipher;
  ParametersWithIV params = new ParametersWithIV(keyParam, iv);
  switch (mode) {
    case CBC_MODE:
      cipher = new CBCBlockCipher(aes);
      break;
    case CFB_MODE:
      cipher = new CFBBlockCipher(aes, aes.blockSize);
      break;
    default:
      throw new ArgumentError('incorrect value of the "mode" parameter');
      break;
  }
  cipher.init(true, params);

  Uint8List textBytes = createUint8ListFromString(plaintext);
  Uint8List paddedText = pad(textBytes, aes.blockSize);
  Uint8List cipherBytes = _processBlocks(cipher, paddedText);
  Uint8List cipherIvBytes = new Uint8List(cipherBytes.length + iv.length)
    ..setAll(0, iv)
    ..setAll(iv.length, cipherBytes);

  return base64.encode(cipherIvBytes);*/

  /* IvParameterSpec iv = new IvParameterSpec(vect.getBytes("UTF-8"));
  SecretKeySpec skeySpec = new SecretKeySpec(chabbi.getBytes("UTF-8"), "AES");

  Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
  cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

  byte[] encrypted = cipher.doFinal(value.getBytes());

    return Base64.encodeToString(encrypted, Base64.DEFAULT);*/

}

String returnMixupString(String token, String reverseEng) {
  StringBuffer demo = new StringBuffer();
  int x = 0;
  int y = 0;
  print(reverseEng.length * 2);
  for (int i = 0; i < token.length + reverseEng.length; i++) {
    if (i < reverseEng.length * 2) {
      if (i % 2 == 0) {
        demo.write(reverseEng[x]);
//print("value of stsmp"+reverseEng[x]);
        // demo.append(ranmo.charAt(x));
        x++;
      } else {
        demo.write(token[y]);
        //print("Actual token"+token[y]);
        y++;
      }
    } else {
      demo.write(token[y]);
      //print("RemainingParts"+token[y]);
      y++;
    }
  }
  return demo.toString();
}
