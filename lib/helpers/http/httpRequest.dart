import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'EncrptMe.dart';

class HttpRequestProcess {
  EncrptMe encrptMe = new EncrptMe();

  Future<HttpClientResponse> requestProcess(
      BuildContext context, String url, String method) async {
    print('$url,$method,$context');
    
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String tok = prefs.getString("token");
      var client = await encrptMe.genrateClient(context);
      final request = method == "get"
          ? await client.getUrl(Uri.parse(url))
          : await client.postUrl(Uri.parse(url));
      request.headers.set(
          HttpHeaders.contentTypeHeader, "application/json; charset=UTF-8");
      request.headers.set("Authorization", tok);
      request.headers.set("blen", "2");
      return await request.close();
    
  }

  Future<HttpClientResponse> requestProcessWithParams(BuildContext context,
      String url, String method, Map<String, String> params) async {
    print('$url,$method,$context');
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String tok = prefs.getString("token");
      var client = await encrptMe.genrateClient(context);
      final request = method == "get"
          ? await client.getUrl(Uri.parse(url))
          : await client.postUrl(Uri.parse(url));
      request.headers.set(
          HttpHeaders.contentTypeHeader, "application/json; charset=UTF-8");
      request.headers.set("Authorization", tok);
      request.headers.set("blen", "2");
      request.write(json.encode(params));
      return await request.close();
    } catch (e) {
      print('$e');
      return e;
    }
  }
}
