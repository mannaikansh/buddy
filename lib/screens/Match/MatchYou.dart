import 'dart:ui';
import '../../helpers/Dummy_Data.dart';
import '../all_screen.dart';

class MatchYouScreen extends StatefulWidget {
  MatchYouScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MatchYouScreenState createState() => _MatchYouScreenState();
}

class _MatchYouScreenState extends State<MatchYouScreen>
    with AutomaticKeepAliveClientMixin<MatchYouScreen> {
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      height: double.infinity,
      padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: double.infinity,
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Theme.of(context).primaryColor,
            ),
            child: MaterialButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              height: 40,
              elevation: 0,
              onPressed: () {
                Navigator.of(context).pushNamed(MessagesScreen.routeName);
              },
              child: Text('Send Message to your partner'),
              textColor: Colors.white,
            ),
          ),

          SizedBox(
            height: 20,
          ),

          Card(
            elevation: 5,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(30),
              ),
              child: TextField(
                cursorColor: Colors.grey,
                decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.search,
                    color: Colors.grey,
                  ),
                  hintText: 'Search Your Matches',
                  hintStyle: TextStyle(color: Colors.grey),
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                ),
                style: TextStyle(color: Colors.black),
              ),
            ),
          ),

          SizedBox(
            height: 20,
          ),

          Padding(
            padding: const EdgeInsets.only(left: 10.0),
            child: Text(
              'Matches You',
              style: TextStyle(
                fontSize: 17,
              ),
            ),
          ),
          SizedBox(height: 15),

          // ignore: missing_return
          Expanded(
            child: Container(
              width: double.infinity,
              child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: chatHome.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () =>
                        Navigator.of(context).pushNamed(ChatScreen.routeName),
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: ListTile(
                        title: Text(chatHome[index]['Name'] +
                            ', ' +
                            chatHome[index]['Age'].toString()),
                        leading: Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50)),
                            child: ClipOval(
                              child: Image.network(
                                chatHome[index]['CircleImage'],
                                fit: BoxFit.cover,
                              ),
                            )),
                        subtitle: Row(children: [
                          Icon(
                            Icons.location_on,
                            size: 15,
                          ),
                          Text(chatHome[index]['Distance'].toString() + 'Km')
                        ]),
                        trailing: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            chatHome[index]['IsUserActive'] == true
                                ? FaIcon(
                                    FontAwesomeIcons.solidCircle,
                                    size: 10,
                                    color: Colors.green,
                                  )
                                : Container(
                                    child: Text(''),
                                  ),
                            SizedBox(
                              height: 10,
                            ),
                            Icon(
                              FontAwesomeIcons.heartbeat,
                              size: 15,
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
