import 'package:buddy/helpers/Dummy_Data.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../all_screen.dart';
import '../../Providers/ProfileProvider.dart';
import 'package:flutter/cupertino.dart';

class ProfilePage extends StatefulWidget {
  static const routeName = "/profile-screen";
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  int points = 0;
  String _userid;

  void setPrefixes() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
    _userid = preferences.getString("userid");      
    });

  }

  @override
  void initState() {
    setPrefixes();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget custCard({String values, String title, IconData custIcon}) {
    return Expanded(
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Container(
          height: 100,
          child: Stack(
            children: [
              Positioned(
                bottom: 40,
                left: 10,
                child: Text(
                  values,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 30,
                  ),
                ),
              ),
              Positioned(
                bottom: 20,
                left: 10,
                child: Text(
                  title,
                  style: TextStyle(
                    fontSize: 17,
                    color: Colors.black45,
                  ),
                ),
              ),
              Positioned(
                right: 10,
                top: 10,
                child: Icon(
                  custIcon,
                  color: Colors.black,
                  size: 30,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget custButton(context, String buttonName) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: 10,
      ),
      height: 40,
      width: double.infinity,
      decoration: BoxDecoration(
          // borderRadius: BorderRadius.circular(10),
          color: Theme.of(context).primaryColor),
      child: MaterialButton(
        onPressed: () {},
        child: Text(buttonName),
        textColor: Colors.white,
        height: 40,
      ),
    );
  }

  Widget customInfo({String first, IconData icon, String second}) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          child: Row(
            children: [
              Icon(icon, color: Colors.grey, size: 15),
              SizedBox(width: 10),
              Text('$first'),
              Expanded(
                flex: 1,
                child: Text('$second', textAlign: TextAlign.end),
              )
            ],
          ),
        ),
        Divider(),
      ],
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: Provider.of<ProfileProvider>(context, listen: false)
            .getProfileDetail(context, _userid),
        builder: (ctx, dataSnapshot) {
          print(dataSnapshot);
          if (dataSnapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CupertinoActivityIndicator(
                radius: 16,
              ),
            );
          } else if (dataSnapshot.error != null) {
            return Center(
              child: Text(
                '${dataSnapshot.error.toString()}',
              ),
            );
          } else {
            return Consumer<ProfileProvider>(builder: (ctx, data, child) {
              final users = data.getProfile;
              print('users : ${users.code} ${users.message} ${users.profile[0].userName}');
              if (users.code == 1 && users.profile.length > 0) {
                return CustomScrollView(
                  slivers: [
                    SliverAppBar(
                      expandedHeight: MediaQuery.of(context).size.height * 0.5,
                      pinned: true,
                      flexibleSpace: FlexibleSpaceBar(
                        title: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              '${users.profile[0].userName}, ${users.profile[0].userAge}',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                            Text(
                              'Ambala',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                        background: Stack(
                          children: [
                            Container(
                              width: double.infinity,
                              child: Image.network(
                                chatHome[6]['CircleImage'],
                                fit: BoxFit.cover,
                              ),
                            ),
                            Container(
                              color: const Color.fromRGBO(0, 0, 0, 0.7),
                            ),
                            Center(
                              child: Container(
                                width: 150,
                                height: 150,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(150),
                                ),
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Colors.white),
                                    shape: BoxShape.circle,
                                  ),
                                  child: ClipOval(
                                    child: Container(
                                      width: 150,
                                      height: 150,
                                      child: Image.network(
                                        chatHome[6]['CircleImage'],
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SliverList(
                      delegate: SliverChildListDelegate(
                        [
                          SizedBox(
                            height: 10,
                          ),
                          custButton(context, 'Invite Friends and earn points'),
                          Container(
                            margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                            child: Row(
                              children: [
                                custCard(
                                  values: '150',
                                  title: 'All matches',
                                  custIcon: FontAwesomeIcons.heartbeat,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                custCard(
                                  values: '30',
                                  title: 'UnMatched Me',
                                  custIcon: FontAwesomeIcons.sadTear,
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                            child: Row(
                              children: [
                                custCard(
                                  values: '500',
                                  title: 'Likes me',
                                  custIcon: FontAwesomeIcons.solidHeart,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                custCard(
                                  values: '50',
                                  title: 'Likes by me ',
                                  custIcon: FontAwesomeIcons.smileWink,
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          customInfo(
                            first: 'D.O.B.',
                            second: '02/06/1999',
                            icon: Icons.cake,
                          ),
                          customInfo(
                            first: 'City',
                            second: 'Karnal',
                            icon: Icons.location_city,
                          ),
                          customInfo(
                            first: 'State',
                            second: 'Haryana',
                            icon: Icons.my_location,
                          ),
                          customInfo(
                            first: 'Instagram',
                            second: 'iammunishmehta',
                            icon: FontAwesomeIcons.instagram,
                          ),
                          customInfo(
                            first: 'Contact',
                            second: '70156-24643',
                            icon: FontAwesomeIcons.phoneAlt,
                          ),
                          customInfo(
                            first: 'Gender',
                            second: 'Male',
                            icon: FontAwesomeIcons.male,
                          ),
                          customInfo(
                            first: 'Interested in',
                            second: 'Women',
                            icon: FontAwesomeIcons.female,
                          ),                        ],
                      ),
                    ),
                  ],
                );
              } else {
                return Center(
                  child: Text(
                    '${users.message}',
                  ),
                );
              }
            });
          }
        },
      ),
    );
  }
}
