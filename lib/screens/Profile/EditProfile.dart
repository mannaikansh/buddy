import 'dart:convert';
import 'dart:io';
import 'package:buddy/helpers/all_helpers.dart';
import 'package:file_picker/file_picker.dart';
import '../all_screen.dart';
import '../../main.dart';

class EditProfile extends StatefulWidget {
  static const routeName = "/edit-profile";
  @override
  _EditProfileState createState() => _EditProfileState();
}

bool maleStatus = false;
bool femaleStatus = false;

class _EditProfileState extends State<EditProfile> {
  TextEditingController name = TextEditingController();
  TextEditingController bio = TextEditingController();
  TextEditingController passion = TextEditingController();
  TextEditingController city = TextEditingController();
  TextEditingController contact = TextEditingController();
  TextEditingController state = TextEditingController();
  TextEditingController dateOfBirth = TextEditingController();
  TextEditingController gender = TextEditingController();

  DateTime _date;

  AgeDuration age;

  File file;
  String fileEncodedString;

  Future _imagePick(context) async {
    file = await FilePicker.getFile(
        // type: FileType.custom,
        // allowedExtensions: ['.pdf'],
        );

    // print('File : $file');
    if (file != null) {
      int _len = file.lengthSync();
      String _fileExtension =
          file.path.split('.')[file.path.split('.').length - 1].toString();
      // print('$_fileExtension : Extension');
      final _bytes = File(file.path).readAsBytesSync();
      if (_fileExtension == "pdf") {
        if (_len < 2097152) {
          // print('File : ok');
          setState(() {
            fileEncodedString = base64Encode(_bytes);
          });
        } else {
          setState(() {
            //_settingModalBottomSheet(context);
            file = null;
          });
        }
      } else {
        setState(() {
          //_settingModalBottomSheet(context);
          file = null;
        });
      }
    }
  }

  void dispose() {
    super.dispose();
  }

  Future<Null> _selectDate(BuildContext context) async {
    DateTime _datePicker = await showRoundedDatePicker(
      context: context,
      initialDate: _date != null ? _date : DateTime.now(),
      firstDate: DateTime(1947),
      lastDate: DateTime.now(),
    );

    if (_datePicker != null && _datePicker != _date) {
      setState(() {
        _date = _datePicker;
      });
    }
  }

  void submitData() {
    setState(() {
      name.text.isNotEmpty ? myName = name.text : Container();
      bio.text.isNotEmpty ? myBio = bio.text : Container();
      passion.text.isNotEmpty ? myPassion = passion.text : Container();
      city.text.isNotEmpty ? myCity = city.text : Container();
      state.text.isNotEmpty ? myState = state.text : Container();
      contact.text.isNotEmpty ? myContact = contact.text : Container();
      gender.text.isNotEmpty ? myGender = gender.text : Container();
      dateOfBirth.text.isNotEmpty ? myDOB = dateOfBirth.text : Container();

      if (_date != null) {
        age = Age.dateDifference(
          fromDate: _date,
          toDate: DateTime.now(),
        );
        myAge = age.years;
      }
    });
  }

  Widget instagramMode({
    String showUser,
    LinearGradient myColors,
    String instaButtonName,
  }) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.symmetric(
        horizontal: 10,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            showUser,
            style: TextStyle(
              fontSize: 17,
              color: Colors.black54,
            ),
          ),
          Container(
            height: 35,
            margin: EdgeInsets.symmetric(vertical: 5),
            decoration: BoxDecoration(
              gradient: myColors,
              borderRadius: BorderRadius.circular(10),
            ),
            padding: EdgeInsets.zero,
            child: FlatButton.icon(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              color: Colors.transparent,
              label: Text(
                instaButtonName,
                style: TextStyle(color: Colors.white),
              ),
              icon: Icon(
                FontAwesomeIcons.instagram,
                color: Colors.white,
              ),
              onPressed: () {
                connectDisconnectInsta();
              },
            ),
          )
        ],
      ),
    );
  }

  void connectDisconnectInsta() {
    setState(() {
      if (isInstaGramConnected == true) {
        isInstaGramConnected = false;
      } else {
        isInstaGramConnected = true;
      }
    });
  }

  Widget textWidget(
    String textName,
  ) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 10, 0, 10),
      child: Text(
        textName,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 16,
        ),
      ),
    );
  }

  String radioItemMe = '';

  Widget textInputArea({
    int lines,
    String showText,
    TextEditingController inputControl,
    String checkValue,
  }) {
    return Container(
      color: Colors.white,
      width: double.infinity,
      child: checkValue != null && checkValue.isNotEmpty
          ? Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 15,
                vertical: 10,
              ),
              child: Text(
                checkValue,
                style: TextStyle(
                  fontSize: 17,
                  color: Colors.black54,
                ),
              ),
            )
          : TextField(
              maxLines: lines,
              controller: inputControl,
              cursorColor: Colors.blueGrey,
              decoration: InputDecoration(
                hintText: showText,
                contentPadding: EdgeInsets.symmetric(horizontal: 10),
                fillColor: Colors.white,
                border: InputBorder.none,
                disabledBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                focusedBorder: InputBorder.none,
                focusedErrorBorder: InputBorder.none,
              ),
            ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(255, 255, 255, 0.95),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            title: Text('Edit Profile'),
            centerTitle: true,
            pinned: true,
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () {
                Navigator.of(context).pushNamed(Bottom.routeName);
              },
            ),
            backgroundColor: Theme.of(context).primaryColor,
            expandedHeight: 50.0,
          ),
          SliverToBoxAdapter(
            child: SizedBox(height: 10),
          ),
          SliverGrid(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                childAspectRatio: 3 / 4,
              ),
              delegate: SliverChildBuilderDelegate((
                BuildContext context,
                int index,
              ) {
                return Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  elevation: 5,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Color.fromRGBO(255, 2555, 255, 1),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: InkWell(
                      onTap: () => _imagePick(context),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.camera_enhance,
                            color: Colors.grey,
                          ),
                          SizedBox(height: 10),
                          Text(
                            'Add Image',
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              }, childCount: 9)),
          SliverToBoxAdapter(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: double.infinity,
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(10)),
                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    child: MaterialButton(
                      onPressed: () {},
                      child: Text('Add Media'),
                      color: Theme.of(context).primaryColor,
                      textColor: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  isInstaGramConnected != true
                      ? instagramMode(
                          showUser: 'Instagram',
                          myColors: LinearGradient(
                            colors: [
                              Colors.blue,
                              Color.fromRGBO(232, 73, 86, 1),
                              Color.fromRGBO(247, 201, 80, 1)
                            ],
                          ),
                          instaButtonName: 'Connect Instagram',
                        )
                      : instagramMode(
                          showUser: instagramUserName,
                          myColors: LinearGradient(
                            colors: [
                              Colors.grey[600],
                              Colors.blueGrey,
                            ],
                          ),
                          instaButtonName: 'Disconnect',
                        ),
                  SizedBox(height: 10),
                  textWidget('Name'),
                  textInputArea(
                    lines: 1,
                    showText: 'Type Full Name',
                    inputControl: name,
                    checkValue: myName,
                  ),
                  SizedBox(height: 10),
                  textWidget('About You'),
                  textInputArea(
                    lines: 1,
                    showText: 'Add Bio',
                    inputControl: bio,
                    checkValue: myBio,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  textWidget('Passion'),
                  textInputArea(
                    lines: 1,
                    showText: 'Add Passion',
                    inputControl: passion,
                    checkValue: myPassion,
                  ),
                  SizedBox(height: 10),
                  textWidget('City'),
                  textInputArea(
                    lines: 1,
                    showText: 'Add City',
                    inputControl: city,
                    checkValue: myCity,
                  ),
                  SizedBox(height: 10),
                  textWidget('State'),
                  textInputArea(
                    lines: 1,
                    showText: 'Add State',
                    inputControl: state,
                    checkValue: myState,
                  ),
                  SizedBox(height: 10),
                  textWidget('Contact'),
                  Container(
                    width: double.infinity,
                    color: Colors.white,
                    child: myContact != null
                        ? Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 15,
                              vertical: 10,
                            ),
                            child: Text(
                              myContact,
                              style: TextStyle(
                                fontSize: 17,
                                color: Colors.black54,
                              ),
                            ),
                          )
                        : TextField(
                            keyboardType: TextInputType.number,
                            controller: contact,
                            maxLength: 10,
                            decoration: InputDecoration(
                              counterText: '',
                              hintText: 'Add Contact Number',
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 10),
                              fillColor: Colors.white,
                              border: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              focusedErrorBorder: InputBorder.none,
                            ),
                          ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  textWidget('Date Of Birth'),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    color: Colors.white,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          _date == null
                              ? 'Choose Your Date Of Birth'
                              : DateFormat().add_yMMMd().format(_date),
                          style: TextStyle(
                            color: Colors.black54,
                            fontSize: 17,
                          ),
                        ),
                        IconButton(
                          icon: Icon(
                            FontAwesomeIcons.calendarAlt,
                            color: Colors.black54,
                            size: 23,
                          ),
                          onPressed: () {
                            _selectDate(context);
                          },
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  textWidget('I Am'),
                  Container(
                    color: Colors.white,
                    child: Column(
                      children: [
                        SizedBox(width: 40),
                        RadioListTile(
                            activeColor: Theme.of(context).primaryColor,
                            value: '0',
                            title: Text('Men'),
                            groupValue: radioItemMe,
                            onChanged: (val) {
                              setState(() {
                                radioItemMe = val;
                              });
                            }),
                        RadioListTile(
                            activeColor: Theme.of(context).primaryColor,
                            value: '1',
                            title: Text('Women'),
                            groupValue: radioItemMe,
                            onChanged: (val) {
                              setState(() {
                                radioItemMe = val;
                              });
                            }),
                        RadioListTile(
                            activeColor: Theme.of(context).primaryColor,
                            value: '2',
                            title: Text('Transgender'),
                            groupValue: radioItemMe,
                            onChanged: (val) {
                              setState(() {
                                radioItemMe = val;
                              });
                            }),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  textWidget('Interested In'),
                  Container(
                    padding: EdgeInsets.only(left: 10),
                    width: double.infinity,
                    color: Colors.white,
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Men'),
                            Switch(
                              activeColor: Theme.of(context).primaryColor,
                              value: maleStatus,
                              onChanged: (value) {
                                setState(
                                  () {
                                    maleStatus = value;
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Women'),
                            Switch(
                              activeColor: Theme.of(context).primaryColor,
                              value: femaleStatus,
                              onChanged: (value) {
                                setState(
                                  () {
                                    femaleStatus = value;
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    width: double.infinity,
                    child: MaterialButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      onPressed: () {
                        submitData();
                      },
                      child: Text('Save Changes'),
                      color: Theme.of(context).primaryColor,
                      textColor: Colors.white,
                      splashColor: Colors.grey,
                      hoverColor: Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
