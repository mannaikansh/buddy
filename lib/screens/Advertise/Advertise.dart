import '../all_screen.dart';

class Advertise extends StatefulWidget {
  static const routeName = "/advertisement";
  @override
  _AdvertiseState createState() => _AdvertiseState();
}

class _AdvertiseState extends State<Advertise> {
  void dispose() {
    super.dispose();
  }

  Widget subscription({
    Color contColor,
    int amount,
    int points,
    int saveValue,
    String header,
  }) {
    return Card(
      elevation: 10,
      margin: EdgeInsets.only(left: 10),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(
          40,
        ),
      ),
      child: Container(
        height: 180,
        width: 150,
        child: Stack(
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Container(
              width: 150,
              height: 180,
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(
                  color: contColor,
                  width: 3,
                ),
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        FontAwesomeIcons.rupeeSign,
                        color: Colors.black54,
                      ),
                      Text(
                        '$amount',
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 30,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  Text(
                    '$points Points',
                    style: TextStyle(
                        color: contColor,
                        fontSize: 22,
                        fontWeight: FontWeight.w500),
                  ),
                  Text(
                    'Save $saveValue%',
                    style: TextStyle(
                        color: Colors.black54,
                        fontSize: 17,
                        fontWeight: FontWeight.w900),
                  ),
                ],
              ),
            ),
            Positioned(
              top: -15,
              child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: contColor,
                  borderRadius: BorderRadius.circular(30),
                ),
                child: Text(
                  '$header',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget getSubscription({
    int days,
    int pointValue,
    double offValue,
    int matchesPerDay,
  }) {
    return Card(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              days == 1 ? '$days Day' : '$days Days',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  color: Colors.black87),
            ),
            Text(
              'You can send \n $matchesPerDay matches per day',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 11,
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MaterialButton(
                  color: Colors.lightBlueAccent,
                  onPressed: () {},
                  child: Text(
                    '$pointValue Points',
                    style: TextStyle(color: Colors.white),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                ),
                offValue != null && offValue != 0
                    ? Text(
                        'Save $offValue%',
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.black54,
                        ),
                      )
                    : Center(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromRGBO(250, 250, 250, 1),
      child: CustomScrollView(
        slivers: [
          SliverAppBar(
            automaticallyImplyLeading: false,
            pinned: true,
            centerTitle: true,
            backgroundColor: Theme.of(context).primaryColor,
            title: Text('Subscription'),
            actions: [
              Container(
                width: 70,
                margin: EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 10,
                ),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(50)),
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Icon(
                      FontAwesomeIcons.coins,
                      color: Colors.deepPurple,
                    ),
                    Text(
                      '0',
                      style: TextStyle(
                          color: Colors.deepPurple,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              )
            ],
          ),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.only(right: 10),
              height: 250,
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    subscription(
                      contColor: Colors.lightBlue,
                      amount: 100,
                      points: 110,
                      saveValue: 10,
                      header: 'Basic',
                    ),
                    subscription(
                      contColor: Colors.redAccent,
                      amount: 180,
                      points: 230,
                      saveValue: 20,
                      header: 'Most Popular',
                    ),
                    subscription(
                      contColor: Colors.lightGreen,
                      amount: 500,
                      points: 600,
                      saveValue: 30,
                      header: 'Best Value 3',
                    ),
                    subscription(
                      contColor: Colors.purple,
                      amount: 900,
                      points: 1000,
                      saveValue: 30,
                      header: 'Best Value 2',
                    ),
                    subscription(
                      contColor: Colors.pinkAccent,
                      amount: 1500,
                      points: 1800,
                      saveValue: 30,
                      header: 'Best Value 1',
                    ),
                  ],
                ),
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                getSubscription(
                  days: 1,
                  pointValue: 10,
                  offValue: 0,
                  matchesPerDay: 5,
                ),
                getSubscription(
                  days: 3,
                  pointValue: 28,
                  offValue: 6.6,
                  matchesPerDay: 7,
                ),
                getSubscription(
                  days: 5,
                  pointValue: 45,
                  offValue: 10,
                  matchesPerDay: 9,
                ),
                getSubscription(
                  days: 7,
                  pointValue: 60,
                  offValue: 14.28,
                  matchesPerDay: 11,
                ),
                getSubscription(
                  days: 14,
                  pointValue: 110,
                  offValue: 21.42,
                  matchesPerDay: 13,
                ),
                getSubscription(
                  days: 30,
                  pointValue: 230,
                  offValue: 23.33,
                  matchesPerDay: 15,
                ),
                getSubscription(
                  days: 90,
                  pointValue: 600,
                  offValue: 33.33,
                  matchesPerDay: 15,
                ),
                getSubscription(
                  days: 180,
                  pointValue: 1000,
                  offValue: 44.44,
                  matchesPerDay: 20,
                ),
                getSubscription(
                  days: 365,
                  pointValue: 1800,
                  offValue: 50.68,
                  matchesPerDay: 20,
                ),
              ],
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 20),
                  Text(
                    'Earn Points',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(top: 5),
                    child: RaisedButton.icon(
                      onPressed: () {},
                      icon: Icon(FontAwesomeIcons.ad),
                      label: Text('Watch Ads'),
                      color: Colors.lightBlue,
                      textColor: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
