import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../all_screen.dart';
import '../../main.dart';

class LoginScreen extends StatelessWidget {
  static const routeName = "/login";

  static final FacebookLogin facebookSignIn = new FacebookLogin();

  Future<Null> _login(BuildContext ctx) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString("userid", "5f79815bd80f9b26c84864e9");
    final FacebookLoginResult result = await facebookSignIn.logIn(['email']);
// final result1 = await facebookSignIn.logInWithReadPermissions(['email']);
    print(preferences.getString('userid'));
    Navigator.of(ctx).pushReplacementNamed(EditProfile.routeName);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final FacebookAccessToken accessToken = result.accessToken;
        print('''
         Logged in!
         Token: ${accessToken.token}
         User id: ${accessToken.userId}
         Expires: ${accessToken.expires}
         Permissions: ${accessToken.permissions}
         Declined permissions: ${accessToken.declinedPermissions}
         ''');
        break;
      case FacebookLoginStatus.cancelledByUser:
        print('Login cancelled by the user.');
        break;
      case FacebookLoginStatus.error:
        print('Something went wrong with the login process.\n'
            'Here\'s the error Facebook gave us: ${result.errorMessage}');
        break;
    }
  }

  Widget authenticationButton(IconData iconName, String btnName,
      Color colorValue, context, Function onpressed) {
    return Container(
      height: 45,
      width: MediaQuery.of(context).size.width * 0.9,
      child: RaisedButton.icon(
        color: colorValue,
        onPressed: onpressed,
        icon: Icon(iconName),
        label: Text(btnName),
        textColor: Colors.white,
        padding: const EdgeInsets.all(12.0),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage('assets/images/background.jpg'),
          ),
          // color: Colors.white,
        ),
        height: double.infinity,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color.fromRGBO(
                      12,
                      13,
                      14,
                      0.5,
                    ),
                    Color.fromRGBO(
                      12,
                      13,
                      14,
                      1,
                    ),
                  ],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                ),
              ),
            ),
            Center(
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 50),
                child: Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Connect Buddy',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 40,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'it\'s easier to join with us now',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  authenticationButton(
                    FontAwesomeIcons.google,
                    'Continue with Google',
                    Color.fromRGBO(
                      233,
                      66,
                      53,
                      1,
                    ),
                    context,
                    () => Navigator.of(context)
                        .pushReplacementNamed(Bottom.routeName),
                  ),
                  Stack(
                    children: [
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        child: Divider(
                          color: Colors.white,
                        ),
                      ),
                      Center(
                        child: Container(
                          color: Colors.black,
                          child: Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 8.0),
                            child: Text(
                              'OR',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  authenticationButton(
                    FontAwesomeIcons.facebookSquare,
                    'Continue with Facebook',
                    Color.fromRGBO(
                      72,
                      103,
                      170,
                      1,
                    ),
                    context,
                    () => _login(context),
                  ),
                  OutlineButton(
                    onPressed: () async {
                      Navigator.of(context)
                          .pushReplacementNamed(Bottom.routeName);
                      SharedPreferences preferences =
                          await SharedPreferences.getInstance();
                      preferences.setString(
                          "userid", "5f79815bd80f9b26c84864e9");
                    },
                    child: Text('Terms & Conditions'),
                    textColor: Colors.white,
                    disabledBorderColor: Colors.transparent,
                    highlightedBorderColor: Colors.transparent,
                  ),
                  // SizedBox(height: 20,),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
