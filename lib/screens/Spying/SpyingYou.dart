import '../all_screen.dart';
import '../../helpers/common.dart';
import '../../widgets/Drawer.dart';

class SpyingYouScreen extends StatefulWidget {
  SpyingYouScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SpyingYouScreenState createState() => _SpyingYouScreenState();
}

class _SpyingYouScreenState extends State<SpyingYouScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      drawer: DrawerWidget(),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(8),
          child: Column(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * .9,
                child: GridView.count(
                    scrollDirection: Axis.vertical,
                    crossAxisCount: 2,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    children: List.generate(
                      10,
                      (index) => InkWell(
                        onTap: () => Common().launchURL(
                            "https://www.amazon.in/ref=as_li_ss_tl?ie=UTF8&linkCode=ll2&tag=shopy20-21&linkId=d82141ee80281f15d2c824e1d13e0354&language=en_IN"),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.all(Radius.circular(90)),
                          ),
                          child: Center(
                            child: FaIcon(
                              FontAwesomeIcons.userAlt,
                              color: Theme.of(context).accentColor,
                            ),
                          ),
                        ),
                      ),
                    )),
              ),
              SizedBox(
                height: 10,
                // child: Text("App Install Section"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
