import 'package:buddy/helpers/all_helpers.dart';

import '../../helpers/Dummy_Data.dart';
import '../all_screen.dart';

class LikeYouScreen extends StatefulWidget {
  LikeYouScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LikeYouScreenState createState() => _LikeYouScreenState();
}

class _LikeYouScreenState extends State<LikeYouScreen>
    with AutomaticKeepAliveClientMixin<LikeYouScreen> {
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      height: double.infinity,
      padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 10,
          ),

          Card(
            elevation: 5,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30),
            ),
            child: Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(30),
              ),
              child: TextField(
                cursorColor: Colors.grey,
                decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.search,
                    color: Colors.grey,
                  ),
                  hintText: 'Search who likes you',
                  hintStyle: TextStyle(color: Colors.grey),
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                ),
                style: TextStyle(color: Colors.black),
              ),
            ),
          ),

          SizedBox(
            height: 20,
          ),

          Padding(
            padding: const EdgeInsets.only(left: 10.0),
            child: Text(
              'Likes Your Profile',
              style: TextStyle(
                fontSize: 17,
              ),
            ),
          ),
          SizedBox(height: 15),

          // ignore: missing_return
          Expanded(
            child: Container(
              width: double.infinity,
              child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: chatHome.length,
                itemBuilder: (context, index) {
                  return Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: ListTile(
                        title: Text(chatHome[index]['Name']),
                        leading: Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50)),
                            child: ClipOval(
                              child: Image.network(
                                chatHome[index]['CircleImage'],
                                fit: BoxFit.cover,
                              ),
                            )),
                        subtitle: SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(children: [
                              Text(
                                'Bio: ',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Text(chatHome[index]['Bio'])
                            ])),
                        trailing: Icon(
                          FontAwesomeIcons.solidGrinHearts,
                          color: Colors.deepPurple,
                        )),
                  );
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
