import '../all_screen.dart';

class Interaction extends StatefulWidget {
  static const routeName = "/interaction";
  @override
  _InteractionState createState() => _InteractionState();
}

class _InteractionState extends State<Interaction>
    with SingleTickerProviderStateMixin {
  String currentTitle;
  TabController _controller;

  void dispose() {
    super.dispose();
  }

  final List<String> allBars = ['Matches', 'Likes', 'Random Active Users'];

  @override
  void initState() {
    currentTitle = allBars[0];
    _controller = TabController(length: 3, vsync: this);
    _controller.addListener(changeTitle);
    super.initState();
  }

  void changeTitle() {
    setState(() {
      currentTitle = allBars[_controller.index];
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          // automaticallyImplyLeading: false,
          title: Text(currentTitle),
          centerTitle: true,

          backgroundColor: Theme.of(context).primaryColor,
          elevation: 0,
          bottom: TabBar(
            controller: _controller,
            tabs: [
              Tab(
                icon: Icon(FontAwesomeIcons.heartbeat),
              ),
              Tab(
                icon: Icon(FontAwesomeIcons.solidGrinHearts),
              ),
              Tab(
                icon: Icon(FontAwesomeIcons.affiliatetheme),
              ),
            ],
          ),
        ),
        body: TabBarView(
          controller: _controller,
          children: [
            MatchYouScreen(),
            LikeYouScreen(),
            SpyingYouScreen(),
          ],
        ),
      ),
    );
  }
}
