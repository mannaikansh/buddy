import 'package:provider/provider.dart';
import 'package:flutter_tindercard/flutter_tindercard.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/cupertino.dart';

import '../../Providers/HomeProvider.dart';
import '../all_screen.dart';

class MyHomePage extends StatefulWidget {
  static const routeName = '/MyHomePageScreen';
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool likeStatus = false;
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future:
            Provider.of<HomeProvider>(context, listen: false).allUsers(context),
        builder: (ctx, dataSnapshot) {
          if (dataSnapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CupertinoActivityIndicator(
                radius: 16,
              ),
            );
          } else if (dataSnapshot.error != null) {
            return Center(
              child: Text(
                '${dataSnapshot.error.toString()}',
              ),
            );
          } else {
            return Consumer<HomeProvider>(
              builder: (ctx, data, child) {
                final users = data.getUsers;
                if (users.code == 1 && users.homeUserList.length > 0) {
                  return Stack(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(50),
                            bottomRight: Radius.circular(50),
                          ),
                          color: Colors.redAccent,
                        ),
                        height: MediaQuery.of(context).size.height * 0.2,
                      ),
                      Container(
                        child: TinderSwapCard(
                          swipeUp: true,
                          swipeDown: true,
                          stackNum: 2,
                          maxWidth: MediaQuery.of(context).size.width,
                          maxHeight: MediaQuery.of(context).size.height,
                          minWidth: MediaQuery.of(context).size.width * 0.8,
                          minHeight: MediaQuery.of(context).size.height * 0.8,
                          totalNum: users.homeUserList.length,
                          swipeCompleteCallback:
                              (CardSwipeOrientation orientation, int index) {
                            // if (orientation == CardSwipeOrientation.LEFT ||
                            //     orientation == CardSwipeOrientation.DOWN) {
                            //   users.doNotShowThisUser(user[index].email);
                            // }
                            // if (orientation == CardSwipeOrientation.RIGHT ||
                            //     orientation == CardSwipeOrientation.UP) {
                            //   users.likeThisUser(user[index].email);
                            // }
                          },
                          swipeUpdateCallback:
                              (DragUpdateDetails details, Alignment align) {
                            if (align.x == 0 || align.y == 0) {
                              likeStatus = false;
                            }
                            if (align.x < 0) {
                              //Card is LEFT swiping
                              likeStatus = false;
                              print('Card is left swiping');
                            } else if (align.x > 0) {
                              //Card is RIGHT swiping
                              likeStatus = true;
                              print('Card is right swiping');
                            }
                            print(
                                'The value of current alignment is ${align.x}');
                            // print('Details is $details');
                          },
                          cardBuilder: (context, index) {
                            return ChangeNotifierProvider(
                              create: (c) {
                                // ignore: unnecessary_statements
                                users.homeUserList[index];
                              },
                              child: SafeArea(
                                child: Container(
                                  margin: EdgeInsets.only(top: 30),
                                  decoration: BoxDecoration(
                                    // color: Colors.white,
                                    borderRadius: BorderRadius.circular(0),
                                  ),
                                  child: Stack(
                                    overflow: Overflow.visible,
                                    children: [
                                      Align(
                                        alignment: Alignment.bottomCenter,
                                        child: Container(
                                          color: Colors.white,
                                          height: 300,
                                        ),
                                      ),
                                      Container(
                                        padding:
                                            const EdgeInsets.only(bottom: 70.0),
                                        child: InkWell(
                                          onTap: () {
                                            showModalBottomSheet(
                                              shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(30),
                                                  topRight: Radius.circular(30),
                                                ),
                                              ),
                                              backgroundColor: Colors.white,
                                              context: context,
                                              builder: (builder) {
                                                return SingleChildScrollView(
                                                  child: Container(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                      vertical: 30,
                                                      horizontal: 25,
                                                    ),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              data
                                                                  .getUsers
                                                                  .homeUserList[
                                                                      index]
                                                                  .userName,
                                                              style: TextStyle(
                                                                  fontSize: 25,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  color: Colors
                                                                      .black54),
                                                            ),
                                                            Text(
                                                              ', ' +
                                                                  data
                                                                      .getUsers
                                                                      .homeUserList[
                                                                          index]
                                                                      .userAge
                                                                      .toString(),
                                                              style: TextStyle(
                                                                  fontSize: 20,
                                                                  color: Colors
                                                                      .black54),
                                                            ),
                                                          ],
                                                        ),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        SingleChildScrollView(
                                                          scrollDirection:
                                                              Axis.horizontal,
                                                          child: Row(
                                                            children: [
                                                              Icon(
                                                                FontAwesomeIcons
                                                                    .smile,
                                                                color:
                                                                    Colors.grey,
                                                              ),
                                                              SizedBox(
                                                                width: 20,
                                                              ),
                                                              Text(
                                                                  'Bio: ' +
                                                                      data
                                                                          .getUsers
                                                                          .homeUserList[
                                                                              index]
                                                                          .userAbout,
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          17,
                                                                      color: Colors
                                                                          .black54))
                                                            ],
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        Row(
                                                          children: [
                                                            Icon(
                                                              FontAwesomeIcons
                                                                  .smileBeam,
                                                              color:
                                                                  Colors.grey,
                                                            ),
                                                            SizedBox(
                                                              width: 20,
                                                            ),
                                                            Text(
                                                              'Passion: ',
                                                              style: TextStyle(
                                                                fontSize: 17,
                                                                color: Colors
                                                                    .black54,
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        Divider(),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        Row(
                                                          children: [
                                                            Icon(
                                                              Icons
                                                                  .not_listed_location,
                                                              color:
                                                                  Colors.grey,
                                                            ),
                                                            SizedBox(
                                                              width: 20,
                                                            ),
                                                            Text(
                                                              // user[index]
                                                              //         .distance
                                                              //         .toString() +
                                                              ' Km Away',
                                                              style: TextStyle(
                                                                fontSize: 17,
                                                                color: Colors
                                                                    .black54,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        SizedBox(height: 10),
                                                        Row(
                                                          children: [
                                                            Icon(
                                                              Icons.location_on,
                                                              color:
                                                                  Colors.grey,
                                                            ),
                                                            SizedBox(
                                                              width: 20,
                                                            ),
                                                            Text('city',
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        17,
                                                                    color: Colors
                                                                        .black54)),
                                                          ],
                                                        ),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        Divider(),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        SizedBox(
                                                          height: 5,
                                                        ),
                                                        Row(
                                                          children: [
                                                            Icon(
                                                              FontAwesomeIcons
                                                                  .instagram,
                                                              color:
                                                                  Colors.grey,
                                                            ),
                                                            SizedBox(width: 20),
                                                            Text(
                                                              '.instagramUserId',
                                                              style: TextStyle(
                                                                fontSize: 17,
                                                                color: Colors
                                                                    .black54,
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                        SizedBox(
                                                          height: 20,
                                                        ),
                                                        Container(
                                                          width:
                                                              double.infinity,
                                                          height: 40,
                                                          decoration:
                                                              BoxDecoration(
                                                            // borderRadius: BorderRadius.circular(10),
                                                            gradient:
                                                                LinearGradient(
                                                              colors: [
                                                                Color.fromRGBO(
                                                                    115,
                                                                    45,
                                                                    161,
                                                                    1),
                                                                Color.fromRGBO(
                                                                    69,
                                                                    3,
                                                                    111,
                                                                    1)
                                                              ],
                                                            ),
                                                          ),
                                                          child:
                                                              FlatButton.icon(
                                                            icon: FaIcon(
                                                                FontAwesomeIcons
                                                                    .shareAlt),
                                                            shape:
                                                                RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          10),
                                                            ),
                                                            // height: 40,
                                                            // elevation: 0,
                                                            onPressed: () {},
                                                            label: Text('Share ' +
                                                                //user[index].name +
                                                                ' Profile'),
                                                            textColor:
                                                                Colors.white,
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        Divider(),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        Container(
                                                            width:
                                                                double.infinity,
                                                            height: 40,
                                                            decoration:
                                                                BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          10),
                                                            ),
                                                            child:
                                                                FlatButton.icon(
                                                              icon: FaIcon(
                                                                  FontAwesomeIcons
                                                                      .disease),
                                                              // shape: RoundedRectangleBorder(
                                                              //   borderRadius:
                                                              //       BorderRadius.circular(10),
                                                              // ),
                                                              onPressed: () {},
                                                              label: Text(
                                                                'Report '
                                                                // +
                                                                //     user[index].name
                                                                ,
                                                              ),
                                                              color: Colors
                                                                  .redAccent,
                                                              textColor:
                                                                  Colors.white,
                                                            ))
                                                      ],
                                                    ),
                                                  ),
                                                );
                                              },
                                            );
                                          },
                                          child:
                                              users.homeUserList[index]
                                                              .images !=
                                                          null &&
                                                      users.homeUserList[index]
                                                              .images.length >
                                                          0
                                                  ? Container(
                                                      child: Carousel(
                                                        showIndicator: false,
                                                        autoplayDuration:
                                                            Duration(
                                                                seconds: 5),
                                                        images:
                                                            users
                                                                .homeUserList[
                                                                    index]
                                                                .images
                                                                .map(
                                                                  (i) =>
                                                                      Container(
                                                                    child:
                                                                        Stack(
                                                                      children: [
                                                                        Container(
                                                                          height: MediaQuery.of(context)
                                                                              .size
                                                                              .height,
                                                                          child:
                                                                              ClipRRect(
                                                                            borderRadius:
                                                                                BorderRadius.circular(40),
                                                                            child:
                                                                                FadeInImage(
                                                                              fit: BoxFit.cover,
                                                                              width: double.infinity,
                                                                              placeholder: AssetImage('assets/images/loading-placeholder.jpg'),
                                                                              image: NetworkImage(i.url),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        Container(
                                                                          decoration:
                                                                              BoxDecoration(
                                                                            borderRadius:
                                                                                BorderRadius.circular(40),
                                                                            gradient:
                                                                                LinearGradient(
                                                                              colors: [
                                                                                Color.fromRGBO(0, 0, 0, 150),
                                                                                Color.fromRGBO(0, 0, 0, 200),
                                                                              ],
                                                                              begin: Alignment.centerLeft,
                                                                              end: Alignment.topLeft,
                                                                            ),
                                                                          ),
                                                                          width:
                                                                              double.infinity,
                                                                          height: MediaQuery.of(context)
                                                                              .size
                                                                              .height,
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                )
                                                                .toList(),
                                                      ),
                                                    )
                                                  : Text(''),
                                        ),
                                      ),
                                      likeStatus == true
                                          ? Positioned(
                                              left: 50,
                                              top: 50,
                                              child: Transform.rotate(
                                                angle: 6,
                                                child: Container(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 20,
                                                      vertical: 5),
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    border: Border.all(
                                                        color: Colors.green,
                                                        width: 3),
                                                  ),
                                                  child: Text(
                                                    'Like',
                                                    style: TextStyle(
                                                        color: Colors.green,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 40),
                                                  ),
                                                ),
                                              ),
                                            )
                                          : Container(),
                                      Positioned(
                                        bottom: 70,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Container(
                                          margin: const EdgeInsets.symmetric(
                                              horizontal: 20.0),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    // user[index].name +
                                                    ',',
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 20,
                                                    ),
                                                    // textAlign: TextAlign.center,
                                                  ),
                                                  Text(
                                                    'user[index].age.toString()',
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 5,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Icon(
                                                    Icons.location_on,
                                                    color: Theme.of(context)
                                                        .accentColor,
                                                  ),
                                                  Text(
                                                    'user[index].city',
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 16,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 20,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                        bottom: 0,
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.9,
                                          // color: Colors.white,
                                          // padding:
                                          //     EdgeInsets.symmetric(vertical: 1),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Expanded(
                                                child: IconButton(
                                                  icon: Image.asset(
                                                    'assets/images/cross.png',
                                                    height: 50,
                                                  ),
                                                  onPressed: () {
                                                    // users.doNotShowThisUser(
                                                    //     user[index].email);
                                                  },
                                                  iconSize: 50,
                                                ),
                                              ),
                                              Expanded(
                                                child: IconButton(
                                                  icon: Image.asset(
                                                    'assets/images/star.png',
                                                    height: 50,
                                                  ),
                                                  // FaIcon(
                                                  //     FontAwesomeIcons.grinHearts),
                                                  onPressed: () {
                                                    // users.superLikeThisUser(
                                                    //     user[index].email);
                                                  },
                                                  iconSize: 50,
                                                ),
                                              ),
                                              Expanded(
                                                child: IconButton(
                                                  icon: Image.asset(
                                                    'assets/images/like.png',
                                                    height: 50,
                                                  ),
                                                  // FaIcon(FontAwesomeIcons.star),
                                                  onPressed: () {
                                                    // users.likeThisUser(
                                                    //     user[index].email);
                                                  },
                                                  iconSize: 50,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                      Positioned(
                        top: 25,
                        right: 10,
                        child: IconButton(
                          tooltip: 'Chats',
                          icon: Image.asset('assets/images/chat.png'),
                          iconSize: 50,
                          onPressed: () {
                            Navigator.of(context)
                                .pushNamed(MessagesScreen.routeName);
                          },
                        ),
                      ),
                    ],
                  );
                } else {
                  return Center(
                    child: Text(
                      '${users.message}',
                    ),
                  );
                }
              },
            );
          }
        },
      ),
    );
  }
}
