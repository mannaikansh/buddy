import 'dart:ui';
import 'package:buddy/helpers/all_helpers.dart';
import '../all_screen.dart';
import 'package:provider/provider.dart';

class SpyingYouScreen extends StatefulWidget {
  SpyingYouScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SpyingYouScreenState createState() => _SpyingYouScreenState();
}

class _SpyingYouScreenState extends State<SpyingYouScreen>
    with AutomaticKeepAliveClientMixin<SpyingYouScreen> {
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    super.build(context);
    final activeUser = Provider.of<ShowUsers>(context);
    final activeUsers = activeUser.showRandomActiveUser;
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 20,
          ),
          Card(
            margin: EdgeInsets.symmetric(horizontal: 10),
            elevation: 5,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30),
            ),
            child: Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(30),
              ),
              child: TextField(
                cursorColor: Colors.grey,
                decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.search,
                    color: Colors.grey,
                  ),
                  hintText: 'Search Active Users',
                  hintStyle: TextStyle(
                    color: Colors.grey,
                  ),
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                ),
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 10.0,
              top: 20,
              bottom: 10,
            ),
            child: Text(
              'Random Active Users',
              style: TextStyle(
                fontSize: 17,
              ),
            ),
          ),
          activeUsers.isNotEmpty
              ? Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 10,
                    ),
                    child: GridView.builder(
                      itemCount: activeUsers.length,
                      itemBuilder: (context, index) {
                        return Stack(
                          children: [
                            Container(
                                width: MediaQuery.of(context).size.width / 2,
                                height: MediaQuery.of(context).size.height / 3,
                                child: ClipRRect(
                                  child: Image.network(
                                    activeUsers[index].imageUrl[0],
                                    fit: BoxFit.cover,
                                  ),
                                  borderRadius: BorderRadius.circular(20),
                                )),
                            Container(
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  colors: [
                                    Color.fromRGBO(0, 0, 0, 200),
                                    Color.fromRGBO(0, 0, 0, 100),
                                  ],
                                  begin: Alignment.centerLeft,
                                  end: Alignment.bottomLeft,
                                ),
                                borderRadius: BorderRadius.circular(20),
                              ),
                              width: double.infinity,
                              height: double.infinity,
                            ),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    activeUsers[index].name,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 15,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      IconButton(
                                          iconSize: 35,
                                          icon: Image.asset(
                                            'assets/images/like.png',
                                            height: 35,
                                            width: 35,
                                          ),
                                          onPressed: () {
                                            activeUser.likeThisUser(
                                                activeUsers[index].email);
                                          }),
                                      IconButton(
                                        iconSize: 35,
                                        icon: Image.asset(
                                          'assets/images/star.png',
                                          height: 35,
                                          width: 35,
                                        ),
                                        onPressed: () {
                                          activeUser.superLikeThisUser(
                                              activeUsers[index].email);
                                        },
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                            )
                          ],
                        );
                      },
                      scrollDirection: Axis.vertical,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10,
                      ),
                    ),
                  ),
                )
              : Expanded(
                  child: Center(
                    // width: double.infinity,
                    // padding: EdgeInsets.only(top: 100),
                    child: Text(
                      'No More Data Found',
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
        ],
      ),
    );
  }
}
