import 'package:flutter/material.dart';

import '../all_screen.dart';
import '../../helpers/Dummy_Data.dart';

class SettingsScreen extends StatelessWidget {
  static const routeName = '/SettingsScreen';

    logoutPopup(BuildContext context) {
    Widget cancelButton = RaisedButton.icon(
      color: Theme.of(context).primaryColor,
      icon: FaIcon(
        FontAwesomeIcons.times,
        size: 14,
      ),
      label: Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = RaisedButton.icon(
      color: Theme.of(context).primaryColor,
      icon: FaIcon(
        FontAwesomeIcons.signOutAlt,
        size: 14,
      ),
      label: Text("Logout"),
      onPressed: () {
        // getOut();
        Navigator.of(context).pop();
        Navigator.of(context).pushReplacementNamed(LoginScreen.routeName);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Logout"),
      content: Text("Are You Sure Want To Logout?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  permanentlyDeleteAccountPopup(BuildContext context) {
    Widget cancelButton = RaisedButton.icon(
      color: Theme.of(context).primaryColor,
      icon: FaIcon(
        FontAwesomeIcons.times,
        size: 14,
      ),
      label: Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = RaisedButton.icon(
      color: Theme.of(context).primaryColor,
      icon: FaIcon(
        FontAwesomeIcons.trash,
        size: 14,
      ),
      label: Text("Delete"),
      onPressed: () {
        // getOut();
        Navigator.of(context).pop();
        Navigator.of(context).pushReplacementNamed(LoginScreen.routeName);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Permanently Delete ?"),
      content: Text(
          "Are You Sure Want To Delete? Once you Delete your Account you can not be recovered."),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }


  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverToBoxAdapter(
          child: SafeArea(
            child: Container(),
          ),
        ),
        SliverToBoxAdapter(
          child: Container(
            color: Colors.grey[100],
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 25),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Container(
                      height: 60,
                      width: 60,
                      // decoration: BoxDecoration(
                      //   border: Border.all(
                      //     color: Theme.of(context).primaryColor,
                      //     // width: 1,
                      //   ),
                      //   shape: BoxShape.circle,
                      // ),
                      child: ClipOval(
                        child: Image.network(
                          chatHome[6]['CircleImage'],
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Pro Member',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 10,
                      ),
                    )
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Munish Mehta',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      '7015624643',
                      style: TextStyle(
                        fontSize: 12,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'iammunishmehta@gmail.com',
                      style: TextStyle(
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
                Flexible(
                  child: IconButton(
                    icon: Icon(
                      Icons.arrow_forward_ios,
                      size: 16,
                    ),
                    onPressed: () {
                      Navigator.of(context).pushNamed(ProfilePage.routeName);
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate(
            [
              SizedBox(
                height: 10,
              ),
              ListTile(
                leading: ImageIcon(
                  AssetImage('assets/images/chat.png'),
                  size: 25,
                  color: Theme.of(context).primaryColor,
                ),
                title: Text('Messages'),
                onTap: () {
                  Navigator.of(context).pushNamed(MessagesScreen.routeName);
                },
                trailing: Icon(
                  Icons.arrow_forward_ios,
                  size: 14,
                ),
              ),
              Divider(),
              ListTile(
                leading: Icon(
                  Icons.verified_user,
                  size: 25,
                  color: Theme.of(context).primaryColor,
                ),
                title: Text('Privacy Policy'),
                onTap: () {
                  Navigator.of(context)
                      .pushNamed(PrivacyPolicyScreen.routeName);
                },
                trailing: Icon(
                  Icons.arrow_forward_ios,
                  size: 14,
                ),
              ),
              Divider(),
              ListTile(
                leading: Icon(
                  Icons.info,
                  color: Theme.of(context).primaryColor,
                  size: 25,
                ),
                title: Text('About Us'),
                onTap: () {
                  Navigator.of(context).pushNamed(AboutUsScreen.routeName);
                },
                trailing: Icon(
                  Icons.arrow_forward_ios,
                  size: 14,
                ),
              ),
              Divider(),
              ListTile(
                leading: Icon(
                  Icons.live_help,
                  color: Theme.of(context).primaryColor,
                  size: 25,
                ),
                title: Text('Help'),
                onTap: () {
                  Navigator.of(context).pushNamed(HelpScreen.routeName);
                },
                trailing: Icon(
                  Icons.arrow_forward_ios,
                  size: 14,
                ),
              ),
              Divider(),
              ListTile(
                leading: Icon(
                  Icons.contacts,
                  color: Theme.of(context).primaryColor,
                  size: 25,
                ),
                title: Text('Support'),
                onTap: () {
                  Navigator.of(context).pushNamed(SupportScreen.routeName);
                },
                trailing: Icon(
                  Icons.arrow_forward_ios,
                  size: 14,
                ),
              ),
              Divider(),
              ListTile(
                leading: Icon(
                  Icons.format_quote,
                  color: Theme.of(context).primaryColor,
                  size: 25,
                ),
                title: Text('FAQ'),
                onTap: () {
                  Navigator.of(context).pushNamed(FAQScreen.routeName);
                },
                trailing: Icon(
                  Icons.arrow_forward_ios,
                  size: 14,
                ),
              ),
                            Divider(),
              ListTile(
                leading: Icon(
                  Icons.exit_to_app,
                  color: Theme.of(context).primaryColor,
                  size: 25,
                ),
                title: Text('Logout'),
                onTap: () => logoutPopup(context),
                trailing: Icon(
                  Icons.arrow_forward_ios,
                  size: 14,
                ),
              ),
                            Divider(),
              ListTile(
                leading: Icon(
                  Icons.delete,
                  color: Theme.of(context).primaryColor,
                  size: 25,
                ),
                title: Text('Delete My Account'),
                onTap: ()=> permanentlyDeleteAccountPopup(context),
                trailing: Icon(
                  Icons.arrow_forward_ios,
                  size: 14,
                ),
              ),
              Container(
                color: Colors.grey[100],
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Text(
                      'Version : 0.0.1',
                      style: TextStyle(color: Colors.grey[600]),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
