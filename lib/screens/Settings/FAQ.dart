import 'package:flutter/material.dart';

class FAQScreen extends StatefulWidget {
  static const routeName="/faq-screen";
  @override
  _FAQScreenState createState() => _FAQScreenState();
}

class _FAQScreenState extends State<FAQScreen> {
  bool status = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            title: Text('FAQ'),
            centerTitle: true,
            backgroundColor: Theme.of(context).primaryColor,
          ),
          SliverToBoxAdapter(
            child: Text('FAQ\'s'),
          ),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.all(5),
              child: Text('''

When match is done by both side:

•	You will be able to see your match person Contact no.
•	You can send message to your match.

When you are buddy pro member:

•	No ads Show  
•	You can send matches
•	Unlimited messages send to your matches anytime
•	You can see his/her Instagram username

When you are buddy VIP member (When  you receive 10,000 likes):

•	Lifetime subscription free
•	No ads show
•	Verified tick
•	Message to anyone without match
•	Send Unlimited Matches'''),
            ),
          ),
        ],
      ),
    );
  }
}
