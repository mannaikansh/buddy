import 'package:buddy/screens/all_screen.dart';
import '../../helpers/Dummy_Data.dart';
import 'package:intl/intl.dart';

class MessagesScreen extends StatefulWidget {
  static const routeName = "/message";
  @override
  _MessagesScreenState createState() => _MessagesScreenState();
}

class _MessagesScreenState extends State<MessagesScreen> {
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            floating: true,
            pinned: true,
            title: Text(
              'Messages',
              style: TextStyle(fontSize: 22),
            ),
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            backgroundColor: Theme.of(context).primaryColor,
          ),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 25, horizontal: 15),
              color: Theme.of(context).primaryColor,
              width: double.infinity,
              child: Container(
                width: MediaQuery.of(context).size.width / 1.2,
                decoration: BoxDecoration(
                  color: Theme.of(context).accentColor,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: TextField(
                  cursorColor: Colors.black,
                  decoration: InputDecoration(
                    prefixIcon: Icon(
                      Icons.search,
                      color: Colors.grey,
                    ),
                    hintText: 'Search Messages',
                    hintStyle: TextStyle(color: Colors.grey),
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                  ),
                  style: TextStyle(color: Colors.black),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              decoration: BoxDecoration(color: Theme.of(context).primaryColor),
              width: double.infinity,
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20),
                    ),
                    color: Colors.white),
                padding: EdgeInsets.fromLTRB(20, 20, 10, 10),
                child: Text(
                  'Active Users',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
              ),
              margin: EdgeInsets.only(bottom: 10),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              width: double.infinity,
              height: 60,
              child: ListView.builder(
                itemCount: chatHome.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return chatHome[index]['IsUserActive'] == true
                      ? Container(
                          width: 60,
                          decoration: BoxDecoration(
                            color: Colors.orange,
                            shape: BoxShape.circle,
                          ),
                          margin: EdgeInsets.symmetric(horizontal: 6),
                          child: Stack(
                            children: [
                              Container(
                                height: 60,
                                width: 60,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Colors.deepPurple,
                                      width: 3,
                                    ),
                                    shape: BoxShape.circle),
                                child: ClipOval(
                                  child: FadeInImage(
                                    placeholder: AssetImage(
                                        'assets/images/loading-placeholder.jpg'),
                                    image: NetworkImage(
                                      chatHome[index]['CircleImage'],
                                    ),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              Positioned(
                                  right: 0,
                                  bottom: 0,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Colors.white, width: 2),
                                      color: Colors.green,
                                      shape: BoxShape.circle,
                                    ),
                                    height: 15,
                                    width: 15,
                                  ))
                            ],
                          ),
                        )
                      : Center();
                },
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, index) {
                return ListTile(
                  leading: Container(
                      width: 50,
                      height: 50,
                      child: ClipOval(
                        child: Image.network(
                          chatHome[index]['CircleImage'],
                          fit: BoxFit.cover,
                        ),
                      )),
                  title: Text(chatHome[index]['Name']),
                  subtitle: Text(
                    chatHome[index]['Conversation'],
                  ),
                  trailing: Text(
                    DateFormat.yMd().format(
                      DateTime.now(),
                    ),
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed(ChatScreen.routeName);
                  },
                  hoverColor: Colors.grey,
                );
              },
              childCount: chatHome.length,
            ),
          ),
        ],
      ),
    );
  }
}
