import 'package:buddy/helpers/Dummy_Data.dart';
import 'package:buddy/screens/all_screen.dart';
import 'package:flutter/material.dart';

class ChatScreen extends StatefulWidget {
  static const routeName="/chat-screen";
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  void dispose() {
    super.dispose();
  }

  Widget messages(String text, Color textColor, Color contColor) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 5, 5, 5),
      constraints: BoxConstraints(
        maxWidth: MediaQuery.of(context).size.width * 0.8,
      ),
      decoration: BoxDecoration(
        color: contColor,
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
          ),
        ],
        borderRadius: BorderRadius.circular(20),
      ),
      padding: EdgeInsets.symmetric(
        vertical: 10,
        horizontal: 15,
      ),
      child: Text(
        text,
        style: TextStyle(
          fontSize: 15,
          color: textColor,
        ),
      ),
    );
  }

  List messagelist = [];

  TextEditingController messagecontrol = TextEditingController();

  void addmessages() {
    var addMessage;
    if (messagecontrol.text.length != 0 && messagecontrol.text.isNotEmpty) {
      addMessage = messagecontrol.text;

      setState(() {
        messagelist.insert(0, addMessage);
        messagecontrol.clear();
      });
    }
  }

  List<SendMenuItems> menuItems = [
    SendMenuItems('Photos', Icons.image, Colors.amber),
    // sendMenuItems('Send Gift', FontAwesomeIcons.gift, Colors.green),
    // sendMenuItems('Request Gift', FontAwesomeIcons.gifts, Colors.purple),
  ];

  // ignore: missing_return
  Widget bottomSheet() {
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      context: context,
      builder: (context) {
        return Container(
          height: 100,
          decoration: BoxDecoration(),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 16,
                ),
                Center(
                  child: Container(
                    height: 4,
                    width: 50,
                    color: Colors.grey.shade200,
                  ),
                ),

                // ignore: missing_return
                ListView.builder(
                  itemCount: menuItems.length,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) {
                    return Container(
                      padding: EdgeInsets.only(
                        top: 10,
                        bottom: 10,
                      ),
                      child: ListTile(
                        leading: Container(
                          decoration: BoxDecoration(
                              color: menuItems[index].color.shade100,
                              shape: BoxShape.circle),
                          child: Icon(
                            menuItems[index].icons,
                            size: 20,
                            color: menuItems[index].color.shade400,
                          ),
                          height: 50,
                          width: 50,
                        ),
                        title: Text(menuItems[index].text),
                        onTap: () {},
                      ),
                    );
                  },
                )
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        flexibleSpace: Container(
          height: double.infinity,
          color: Theme.of(context).primaryColor,
          child: SafeArea(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    }),
                ClipOval(
                    child: Image.network(
                  chatHome[2]['CircleImage'],
                  fit: BoxFit.cover,
                  width: 40,
                  height: 40,
                )),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'Munish Mehta',
                  style: TextStyle(
                    fontSize: 17,
                    color: Colors.white,
                  ),
                ),
                SizedBox(
                  width: 3,
                ),
                Container(
                  width: 10,
                  height: 10,
                  decoration: BoxDecoration(
                    color: Colors.green,
                    shape: BoxShape.circle,
                  ),
                )
              ],
            ),
          ),
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.more_vert),
            onPressed: () {},
          ),
        ],
        automaticallyImplyLeading: false,
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).primaryColor,
        onPressed: () => addmessages(),
        child: Icon(
          Icons.send,
          color: Colors.white,
        ),
      ),
      body: Container(
        height: double.infinity,
        child: Stack(
          children: [
            Container(
              color: Color.fromRGBO(112, 134, 158, 0.3),
              child: Container(
                height: double.infinity,
                margin: EdgeInsets.fromLTRB(10, 0, 10, 70),
                // ignore: missing_return
                child: ListView.builder(
                  reverse: true,
                  itemCount: messagelist.length,
                  itemBuilder: (context, index) {
                    return (index % 2 == 0)
                        ? Align(
                            alignment: Alignment.bottomRight,
                            child: messages(messagelist[index], 
                            Colors.white,
                                Theme.of(context).primaryColor),
                          )
                        : Align(
                            alignment: Alignment.bottomLeft,
                            child: messages(
                              messagelist[index],
                              Colors.black,
                              Colors.white,
                            ),
                          );
                  },
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 10),
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        shape: BoxShape.circle,
                      ),
                      width: 40,
                      height: 40,
                      child: IconButton(
                        icon: Icon(
                          Icons.add,
                          size: 20,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          bottomSheet();
                        },
                      ),
                    ),
                    Expanded(
                      child: TextField(
                        controller: messagecontrol,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          focusedErrorBorder: InputBorder.none,
                          hintText: 'Type a Message',
                          contentPadding: EdgeInsets.only(left: 10, right: 80),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SendMenuItems {
  String text;
  IconData icons;
  MaterialColor color;
  SendMenuItems(this.text, this.icons, this.color);
}
